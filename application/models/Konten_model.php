<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class Konten_model extends CI_Model
{
	
	function __construct()
	{
		$this->load->library('m_db');
	}

	function quote_add($nama,$quote)
	{
		$d=array(
		'nama'=>$nama,
		'quote'=>$quote
		);
		if($this->m_db->add_row('quote',$d)==TRUE)
		{
			return true;
		}else{
			return false;
		}
	}

	function quote_edit($IDquote,$nama,$quote)
	{
		$s=array(
		'id_quote'=>$IDquote,
		);
		$d=array(
		'nama'=>$nama,
		'quote'=>$quote
		);
		if($this->m_db->edit_row('quote',$d,$s)==TRUE)
		{
			return true;
		}else{
			return false;
		}
	}

	function quote_delete($IDquote)
	{
		$s=array(
		'id_quote'=>$IDquote,
		);
		
		if($this->m_db->delete_row('quote',$s)==TRUE)
		{
			return true;
		}else{
			return false;
		}
	}

	function artikel_add($jdl,$met,$kat,$pen,$isi,$cover='')
	{
		$v=array(
			'judul'=>$jdl,
			'meta'=>$met,
			'id_kat_blog'=>$kat,
			'isi'=>$isi,
			'penulis'=>$pen,
			'sematkan'=>0
		);
		if($this->m_db->add_row('blog',$v)==TRUE)
		{
			$blogID=$this->m_db->last_insert_id();
			$pathupload=FCPATH.'assets/images/blog/';
			$allowtype="jpg|bmp|png|jpeg";
			$config['upload_path'] = $pathupload;
			$config['allowed_types'] = $allowtype;
			$config['max_size']	= 0;
			$config['max_filename']=0;
			$config['max_width'] = 0;
			$config['max_height'] = 0;
			$config['overwrite']=TRUE;
			if(!empty($cover))
			{
				$this->load->library('upload');
				$this->load->library('m_file');
				$count=1;
				$field="upload";
				for($i=1;$i<=$count;$i++){					
					if (!empty($_FILES[$field.$i]['name'])) {						
						$gambar=$_FILES[$field.$i]['name'];
		        		$ext=pathinfo($gambar,PATHINFO_EXTENSION);
		        		$imgname="blog_".$blogID."-".$i.".".$ext;
		        		$config['file_name'] = $imgname;
		        		$this->upload->initialize($config);
						if ($this->upload->do_upload($field.$i))
						{							
							$sdata=$this->upload->data();
							$folder=$sdata['file_path'];
							$oripath=$sdata['full_path'];
							$imgname=$sdata['orig_name'];														
							$this->m_file->imageThumbs($pathupload,$oripath,$imgname);
							$d2=array(
							'id_blog'=>$blogID,
							'cover'=>$imgname,
							);
							$this->m_db->add_row('blog_cover',$d2);
						}
					}
				}				
			}
			return true;
		}else{
			return false;
		}
	}

	function artikel_edit($blogID,$jdl,$met,$kat,$pen,$isi,$cover='')
	{
		$s=array(
		'id_blog'=>$blogID,
		);
		$d=array(
		'judul'=>$jdl,
		'meta'=>$met,
		'id_kat_blog'=>$kat,
		'penulis'=>$pen,
		'isi'=>$isi,
		);
		if($this->m_db->edit_row('blog',$d,$s)==TRUE)
		{			
			$pathupload=FCPATH.'assets/images/blog/';
			$allowtype="jpg|bmp|png|jpeg";
			$config['upload_path'] = $pathupload;
			$config['allowed_types'] = $allowtype;
			$config['max_size']	= 0;
			$config['max_filename']=0;
			$config['max_width'] = 0;
			$config['max_height'] = 0;
			$config['overwrite']=TRUE;
			
			if(!empty($cover))
			{
				$this->m_db->delete_row('blog_cover',array('id_blog'=>$blogID));
				$this->load->library('upload');
				$this->load->library('m_file');
				$count=1;
				$field="upload";
				for($i=1;$i<=$count;$i++){					
					if (!empty($_FILES[$field.$i]['name'])) {						
						$gambar=$_FILES[$field.$i]['name'];
		        		$ext=pathinfo($gambar,PATHINFO_EXTENSION);
		        		$imgname="blog_".$blogID."-".$i.".".$ext;
		        		$config['file_name'] = $imgname;
		        		$this->upload->initialize($config);
						if ($this->upload->do_upload($field.$i))
						{							
							$sdata=$this->upload->data();
							$folder=$sdata['file_path'];
							$oripath=$sdata['full_path'];
							$imgname=$sdata['orig_name'];														
							$this->m_file->imageThumbs($pathupload,$oripath,$imgname);
							$d2=array(
							'id_blog'=>$blogID,
							'cover'=>$imgname,
							);
							$this->m_db->add_row('blog_cover',$d2);
						}
					}else{
						$last=$this->input->post('fupload'.$i);
						$d2=array(
						'id_blog'=>$blogID,
						'cover'=>$last,
						);
						$this->m_db->add_row('blog_cover',$d2);
					}
				}				
			}
			$this->m_db->delete_row('blog_cover',array('id_blog'=>$blogID,'cover'=>''));
			return true;
		}else{
			return false;
		}
	}

	function artikel_delete($id)
	{
		$s=array(
		'id_blog'=>$id,
		);
		if($this->m_db->is_bof('blog_cover',$s)==FALSE)
		{
			$dPhoto=$this->m_db->get_data('blog_cover',$s);
			if(!empty($dPhoto))
			{
				$this->load->library('m_file');
				$pathupload=FCPATH.'assets/images/blog/';
				foreach($dPhoto as $rPhoto)
				{
					$filename=$rPhoto->cover;
					$this->m_file->deleteImage($pathupload,$filename);
				}
			}
			$this->m_db->delete_row('blog',$s);
			return true;
		}else{
			return false;
		}
	}

	function kat_tambah($nama)
	{
		$s=array(
		'nama_kat'=>$nama,
		);
		if($this->m_db->add_row('blog_kategori',$s)==TRUE)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	function kat_edit($IDkat,$nama)
	{
		$s=array(
		'id_kat_blog'=>$IDkat,
		);
		$v=array(
		'nama_kat'=>$nama,
		);
		if($this->m_db->edit_row('blog_kategori',$v,$s)==TRUE)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	function kat_hapus($IDkat)
	{
		$s=array(
		'id_kat_blog'=>$IDkat,
		);
		
		if($this->m_db->delete_row('blog_kategori',$s)==TRUE)
		{
			return true;
		}else{
			return false;
		}
	}

	function set($id)
	{
		$s=array(
		'id_blog'=>$id,
		);
		$d=array(
		'banner'=>1
		);
		if($this->m_db->edit_row('blog',$d,$s)==TRUE)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	function un_set($id)
	{
		$s=array(
		'id_blog'=>$id,
		);
		$d=array(
		'banner'=>0
		);
		if($this->m_db->edit_row('blog',$d,$s)==TRUE)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	function semat($idblog,$idsemat)
	{
		$v1=array(
			'id_blog' =>$idsemat
		);
		$v2=array(
			'sematkan' =>0
		);
		if ($this->m_db->edit_row('blog',$v2,$v1)==TRUE) 
		{
			$s1=array(
			'id_blog' =>$idblog
			);
			$s2=array(
				'sematkan' =>1
			);
			if ($this->m_db->edit_row('blog',$s2,$s1)==TRUE) {
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}

}