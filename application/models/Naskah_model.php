<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class Naskah_model extends CI_Model
{
	
	function __construct()
	{
		$this->load->library('m_db');
	}

	function get_data($where=array(),$order="kode_order ASC")
	{
		$d=$this->m_db->get_data('project',$where,$order);
		return $d;
	}

	function naskah_add($memberid,$judul,$penulis,$penerbit,$kat,$kts,$htm,$wrn,$jml_ctk,$hrg_ctk,$hrg_ttl,$hrg_ksm,$pkt,$hrg_pkt,$a1,$a2,$a3,$b1,$b2,$b3,$c1,$c2,$c3,$sel_alih,$catatan)
	{
		$tgl = date("Y-m-d H:i:s");
		$kd_order = strtotime($tgl);
		$total = $hrg_ttl+$hrg_pkt;
		$val = array(
			'kode_order'=>$kd_order,
			'member_id'=>$memberid,
			'id_paket'=>$pkt,
			'tgl_order'=>$tgl,
			'total'=>$total,
			'status'=>'menunggu'
		);

		if ($this->m_db->add_row('project',$val)==TRUE)
		{
			$projectID=$this->m_db->last_insert_id();
			$val2 = array(
				'id_project'=>$projectID,
				'judul'=>$judul,
				'penulis'=>$penulis,
				'penerbit'=>$penerbit,
				'kat_id'=>$kat,
				'id_kertas'=>$kts,
				'jml_hitam'=>$htm,
				'jml_warna'=>$wrn,
				'harga_satuan'=>$hrg_ctk,
				'harga_konsumen'=>$hrg_ksm,
				'jml_cetak'=>$jml_ctk,
				'catatan'=>$catatan
			);
			if ($this->m_db->add_row('project_detail',$val2)==TRUE) {
				$val3 = array(
					'id_project'=>$projectID,
					'penulisan'=>$a1,
					'penyuntingan'=>$a2,
					'alihbahasa'=>$a3,
					'sampul'=>$b1,
					'tataletak'=>$b2,
					'ilustrasi'=>$b3,
					'isbn'=>$c1,
					'cetak'=>$c2,
					'pemasaran'=>$c3,
					'pilihan_alihbahasa'=>$sel_alih
				);
				$this->m_db->add_row('paket_custom',$val3);
				
				$hsl['status'] = true;
				$hsl['projectID'] = $projectID;
			}
			else {
				return false;
			}
			
		}
		else{
			$hsl['status'] = false;
		}

		return $hsl;
	}

	function naskah_biaya($IDpro,$htm,$wrn,$ctk,$hrg_ctk,$total_ctk,$pkt,$hrg_pkt,$a1,$a2,$a3,$b1,$b2,$b3,$c1,$c2,$c3,$sel_alih)
	{
		$op = $this->m_db->get_data('biaya_operasional',array('id_biaya'=>1));
		foreach ($op as $b) {
		}
		$tot_hlm = $htm + $wrn;
		
			if ($a1==1) {
				$tot_tls = $b->hrg_penulisan * $tot_hlm;
			}
			else{
				$tot_tls = 0;
			}
			if ($a2==1) {
				$tot_sun = $b->hrg_penyuntingan * $tot_hlm;
			}
			else{
				$tot_sun = 0;
			}
			if ($a3==1) {
				if ($sel_alih==1) {
					$tot_alih = $b->hrg_alih_intoeng * $tot_hlm;
				}
				elseif ($sel_alih==2) {
					$tot_alih = $b->hrg_alih_engtoin * $tot_hlm;
				}
				else {
					$tot_alih = 0;
				}
			}
			else{
				$tot_alih = 0;
			}
			if ($b1==1) {
				$tot_rcg = $b->hrg_rancang;
			}
			else{
				$tot_rcg = 0;
			}
			if ($b2==1) {
				$tot_tata = $b->hrg_tata * $tot_hlm;
			}
			else{
				$tot_tata = 0;
			}
			if ($b3==1) {
				$tot_ilus = $b->hrg_ilustrasi;
			}
			else{
				$tot_ilus = 0;
			}
			if ($c1==1) {
				$tot_isbn = $b->hrg_isbn;
			}
			else{
				$tot_isbn = 0;
			}
			if ($c2==1) {
				$tot_cetak = $hrg_ctk;
			}
			else{
				$tot_cetak = $hrg_ctk;
			}
			if ($c3==1) {
				$tot_psr = $b->hrg_pemasaran;
			}
			else{
				$tot_psr = 0;
			}
			$tot_op = $tot_tls + $tot_sun + $tot_alih + $tot_rcg + $tot_tata + $tot_ilus + $tot_isbn + $tot_psr;
			$total_semua = $hrg_pkt + $tot_tls + $tot_sun + $tot_alih + $tot_rcg + $tot_tata + $tot_ilus + $tot_isbn + $tot_psr + $total_ctk;
			$v=array(
				'id_project'=>$IDpro,
				'biaya_paket'=>$hrg_pkt,
				'biaya_tulis'=>$tot_tls,
				'biaya_sunting'=>$tot_sun,
				'biaya_alihbahasa'=>$tot_alih,
				'biaya_rancang'=>$tot_rcg,
				'biaya_tata'=>$tot_tata,
				'biaya_ilustrasi'=>$tot_ilus,
				'biaya_isbn'=>$tot_isbn,
				'biaya_cetak'=>$tot_cetak,
				'biaya_pemasaran'=>$tot_psr,
				'biaya_operasional'=>$tot_op,
				'biaya_totalcetak'=>$total_ctk,
				'total_semua'=>$total_semua
			);
			if ($this->m_db->add_row('project_biaya',$v)==TRUE) {
				return true;
			}
			else {
				return false;
			}
	}

	function file_add($idpro,$naskah,$cover)
	{
		$d=array(
		'id_project'=>$idpro,
		'naskah'=>$naskah,
		'cover'=>$cover
		);
		if($this->m_db->add_row('file_project',$d)==TRUE)
		{
			return true;
		}else{
			return false;
		}
	}

	function getProjectbyID($id) {
		$this->db->select("*");
		$this->db->from("project");
		$this->db->join("project_detail","project.id_project=project_detail.id_project");
		$this->db->where("id_project",$id);
		$hasil = $this->db->get();
		$data = $hasil->row();
		return $data;
	}

	function naskah_data($where=array(),$order="tgl_order ASC")
	{
		$d=$this->m_db->get_data('project',$where,$order);
		return $d;
	}

	function ubah_status_proses($IDpro)
	{
		$s=array(
			'id_project'=>$IDpro
		);
		$v=array(
			'status'=>'proses'
		);
		if ($this->m_db->edit_row('project',$v,$s)==TRUE) {
			return true;
		}
		else{
			return false;
		}
	}

	function ubah_status_selesai($IDpro)
	{
		$s=array(
			'id_project'=>$IDpro
		);
		$v=array(
			'status'=>'selesai'
		);
		if ($this->m_db->edit_row('project',$v,$s)==TRUE) {
			return true;
		}
		else{
			return false;
		}
	}

	function del_psn($id)
	{
		$s=array(
			'id_project'=>$id
		);
		if ($this->m_db->delete_row('project',$s)==TRUE) {
			$IDdtl = field_value('project_detail','id_project',$id,'detail_id');
			$v=array(
				'detail_id'=>$IDdtl
			);
			if ($this->m_db->delete_row('project_detail',$v)==TRUE) {
				return true;
			}
			else {
				return false;
			}
		}
		else {
			return false;
		}
	}
}