<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('m_db');
		$this->load->model('register_model');
	}
	
	function index()
	{
		if(aktif()==1)
		{
			redirect(base_url());
			
		}else{
			redirect(base_url().'member/daftar');
		}		
	}

	function masuk()
	{
        $this->load->view('html/page/v_login');
	}
		
	function daftar()
	{
        $this->load->view('html/page/v_daftar');
	}

	function lupa_sandi()
	{
        $this->load->view('html/page/v_reset');
	}

	function email_check()
	{
		// allow only Ajax request    
        if($this->input->is_ajax_request()) {
        // grab the email value from the post variable.
        $email = $this->input->post('email');
        // check in database - table name : tbl_users  , Field name in the table : email
        if(!$this->form_validation->is_unique($email, 'member.email')) {
        // set the json object as output                  
         $this->output->set_content_type('application/json')->set_output(json_encode(array('message' => 'The email is already taken, choose another one')));
            }
        }
	}

	function submit()
	{
		$email = $this->input->post('email');
		$nama = $this->input->post('nama');
		$username = $this->input->post('username');
		$pass = $this->input->post('pwd');
		$password = md5($this->security->xss_clean($this->input->post('pwd')));
		$tgl = $this->input->post('tgl_lahir');
		$jk = $this->input->post('jk');
		$telp = $this->input->post('telp');
		$fb = $this->input->post('fb');
		$twt = $this->input->post('twt');
		$alm = $this->input->post('alamat');
		$nbank = $this->input->post('n_bank');
		$kbank = $this->input->post('k_bank');
		$norek = $this->input->post('no_rek');
		$namarek = $this->input->post('nama_rek');
		
		$em = array(
			'email' => $email			
		);
		if ($this->m_db->is_bof('member',$em)==TRUE) {
			$usr=array(
				'username'=>$username
			);
			if ($this->m_db->is_bof('member',$usr)==TRUE) {
				if ($this->register_model->register($email,$nama,$username,$password,$tgl,$jk,$telp,$fb,$twt,$alm,$nbank,$kbank,$norek,$namarek)==TRUE) {
					$encrypted_id = md5($this->m_db->last_insert_id());

					$this->load->library('email');
					$config = array();
					$config['charset'] = 'utf-8';
					$config['useragent'] = 'Codeigniter';
					$config['mailtype']= "html";
					$config['protocol']= "smtp";
					$config['smtp_host']= "ssl://smtp.gmail.com";
					$config['smtp_user']= "redaksi.bikinbuku@gmail.com";
					$config['smtp_pass']= "papringan8C";
					$config['smtp_port'] = '465';
					$config['crlf']="\r\n"; 
					$config['newline']="\r\n"; 
				
					$config['wordwrap'] = TRUE;
					//memanggil library email dan set konfigurasi untuk pengiriman email
					$pesan = '
						<html>
					    <head>
					        <title>Selamat Datang di Bikinbuku.co.id</title>
					    </head>
					    <body>
					        <h1>Terimakasih sudah bergabung bersama kami!</h1>
					        <table cellspacing="0" style="border: 2px dashed #FB4314; width: 450px; height: auto; text-align:left;">
					            <tr>
					                <th>Nama:</th><td>'.$nama.'</td>
					            </tr>
					            <tr style="background-color: #e0e0e0;">
					                <th>Email:</th><td>'.$email.'</td>
					            </tr>
					            <tr style="background-color: #e0e0e0;">
					                <th>Nama Pengguna:</th><td>'.$username.'</td>
					            </tr>
					            <tr style="background-color: #e0e0e0;">
					                <th>Kata Sandi:</th><td>'.$pass.'</td>
					            </tr>
					        </table><br>
					        <p>Silakan aktifkan akun Anda melalui link dibawah ini.</p><br><br><br>
					        <a href="http://bikinbuku.co.id/member/verification/'.$encrypted_id.'"><img src="http://bikinbuku.co.id/assets/images/aktifkan.png"></a>
					    </body>
					    </html>
					';
					$this->email->initialize($config);
				//konfigurasi pengiriman
					$this->email->from($config['smtp_user']);
				    $this->email->to($email); //email tujuan. Isikan dengan emailmu!
				    $this->email->subject("Verifikasi Akun");
					$this->email->message($pesan);
					if($this->email->send())
					{
						echo "<script>window.alert('Selamat! Periksa email anda untuk verifikasi !');window.location=('".base_url()."member/daftar')</script>";
					}else
					{
						echo "<script>window.alert('Gagal! Periksa email anda untuk verifikasi !');window.location=('".base_url()."member/daftar')</script>";			
					}
				}
				else {
					set_header_message('danger','Daftar Member','Pendaftaran gagal!');
					redirect(base_url('member/daftar'),'refresh',301);
				}
			}
			else {
				set_header_message('warning','Daftar Member','Username sudah dipakai!');
				redirect(base_url('member/daftar'),'refresh',301);
			}
		}
		else {
			set_header_message('warning','Daftar Member','Alamat surel sudah terdaftar sebagai member Bikinbuku.co.id!');
			redirect(base_url('member/daftar'),'refresh',301);
		}
	}

	function edit_profil()
	{
		$memID = $this->input->post('memberID');
		$email = $this->input->post('email');
		$nama = $this->input->post('nama');
		$username = $this->input->post('username');
		$tgl = $this->input->post('tgl_lahir');
		$jk = $this->input->post('jk');
		$telp = $this->input->post('telp');
		$fb = $this->input->post('fb');
		$twt = $this->input->post('twt');
		$alm = $this->input->post('alamat');
		$nbank = $this->input->post('n_bank');
		$kbank = $this->input->post('k_bank');
		$norek = $this->input->post('no_rek');
		$namarek = $this->input->post('nama_rek');
		if ($this->register_model->edit_profil($memID,$email,$nama,$username,$tgl,$jk,$telp,$fb,$twt,$alm,$nbank,$kbank,$norek,$namarek)==TRUE) {
			set_header_message('success','Edit Profil','Profil Anda berhasil diperbarui');
			redirect(base_url('profil'),'refresh',301);
		}
		else
		{
			set_header_message('danger','Edit Profil','Profil Anda gagal diperbarui');
			redirect(base_url('profil'),'refresh',301);
		}
	}

	function reset()
	{
		$ref = $this->input->get('ref');
		$meta['nav'] = "layanan";
		$meta['judul'] = baca_konfig('bikin-buku')." - Lupa Sandi";
		$meta['data']=$this->m_db->get_data('blog',array('banner'=>1));
		$this->load->view('html/header',$meta);
		$this->load->view('html/page/v_reset');
		$this->load->view('html/footer',$meta);
	}

	function request_reset()
	{
		$email = $this->input->post('email');
		$send = $this->register_model->send_reset($email);
		if ($send['status']==TRUE) {
			$nama = field_value('member','email',$email,'nama');

			$this->load->library('email');
			$config = array();
			$config['charset'] = 'utf-8';
			$config['useragent'] = 'Codeigniter';
			$config['mailtype']= "html";
			$config['protocol']= "smtp";
			$config['smtp_host']= "ssl://smtp.gmail.com";
			$config['smtp_user']= "redaksi.bikinbuku@gmail.com";
			$config['smtp_pass']= "papringan8C";
			$config['smtp_port'] = '465';
			$config['crlf']="\r\n"; 
			$config['newline']="\r\n"; 
		
			$config['wordwrap'] = TRUE;
			//memanggil library email dan set konfigurasi untuk pengiriman email
			
			$this->email->initialize($config);
		//konfigurasi pengiriman
			$this->email->from($config['smtp_user']);
		    $this->email->to($email); //email tujuan. Isikan dengan emailmu!
		    $this->email->subject("Reset Kata Sandi");
			$this->email->message(
			"<p>Hi, ".$nama."!</p><br>Silahkan reset kata sandi akun Anda melalui link dibawah ini.<br><br><br>
				<a href='".base_url()."member/reset_link/".$send['token']."'><img src='".base_url()."assets/images/reset.png'></a>");
			if($this->email->send())
			{
				set_header_message('success','Reset Kata Sandi','Link guna reset kata sandi sudah dikirimkan ke alamat surel Anda! Silahkan cek alamat surel Anda!');
				redirect(base_url('member/reset'),'refresh',301);
			}else
			{
				echo "<script>window.alert('Maaf, pengiriman email gagal!');window.location=('".base_url()."member/reset')</script>";		
			}
		}
		else
		{
			set_header_message('danger','Reset Kata Sandi','Alamat surel Anda tidak terdaftar sebagai member Bikinbuku.co.id!');
			redirect(base_url('member/reset'),'refresh',301);
		}

	}

	function reset_link($key)
	{
		$s=array(
			'token'=>$key
		);
		if ($this->m_db->is_bof('member',$s)==FALSE) {
			$meta['nav'] = "layanan";
			$meta['judul'] = baca_konfig('bikin-buku')." - Reset Kata Sandi";
			$meta['data']=$this->m_db->get_data('blog',array('banner'=>1));
			$data['key'] = field_value('member','token',$key,'id');
			$this->load->view('html/header',$meta);
			$this->load->view('html/page/v_reset_link',$data);
			$this->load->view('html/footer',$meta);
		}
		else
		{
			echo "<script>window.alert('Token Anda tidak valid!');window.location=('".base_url()."member/reset')</script>";
		}
	}

	function apply_reset()
	{
		$IDmem = $this->input->post('IDmember');
		$sandi = md5($this->security->xss_clean($this->input->post('pass')));
		if ($this->register_model->reset_sandi($IDmem,$sandi)==TRUE) {
			echo "<script>window.alert('Kata sandi Anda berhasil diganti!');window.location=('".base_url()."member/reset')</script>";
		}
		else {
			echo "<script>window.alert('Silakan periksa email anda untuk melakukan reset kata sandi!');window.location=('".base_url()."member/reset')</script>";
		}
	}

	function verification($key)
	{
		if ($this->register_model->changeActiveState($key)==TRUE) {
			echo "<script>window.alert('Selamat! Email anda berhasil diverifikasi! Silakan login');window.location=('".base_url()."welcome')</script>";
		}
		else {
			echo "<script>window.alert('Akun Anda gagal diverifikasi!');window.location=('".base_url()."error')</script>";
		}
		
	}
}