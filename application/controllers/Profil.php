<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profil extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('m_db');
	}
	
	function index()
	{
		if(aktif()!=1)
		{
			$ref=$this->uri->ruri_string();
			redirect(base_url().'member/daftar?ref='.$ref);
		}
		else
		{
			$meta['nav'] = "layanan";
			$meta['judul'] = baca_konfig('bikin-buku')." - Profil";
			$meta['data']=$this->m_db->get_data('blog',array('banner'=>1));
			$this->load->view('html/header',$meta);
			$this->load->view('html/page/v_profil');
			$this->load->view('html/footer',$meta);
		}
		
	}

}