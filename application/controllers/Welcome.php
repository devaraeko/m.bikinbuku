<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	}
	
	function index()
	{
		$meta['nav'] = "login";
		$meta['judul'] = baca_konfig('bikin-buku')." - Selamat Datang";
		$meta['data']=$this->m_db->get_data('blog',array('banner'=>1));
		$this->load->view('html/header',$meta);
		$this->load->view('html/page/v_welcome');
		$this->load->view('html/footer',$meta);
	}
}
