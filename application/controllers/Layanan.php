<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Layanan extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('m_db');
	}
	
	function index()
	{
		$meta['nav'] = "layanan";
		$meta['judul'] = baca_konfig('bikin-buku')." - Layanan";
		$meta['data']=$this->m_db->get_data('blog',array('banner'=>1));
		$this->load->view('html/header',$meta);
		$data['paket']=$this->m_db->get_data('paket');
		$this->load->view('html/page/v_layanan',$data);
		$this->load->view('html/footer',$meta);
	}

	function get_detail()
	{
		$id=$this->input->post('id_paket');
		$s=array(
		'id_paket'=>$id
		);
		if ($this->m_db->is_bof('paket',$s)==FALSE) {
			$dtl_paket=$this->m_db->get_data('paket',$s);
			if (!empty($dtl_paket)) {
				foreach ($dtl_paket as $r) {
					$nama = $r->nama_paket;
					$harga = $r->harga;
					$desc = $r->fitur;
					$data = '
						<div class="col-md-12">
							<div class="title-area">
								<h3>Paket '.$nama.'</h3>
							</div>
						</div>
						<div class="col-md-12">
							<div class="pull-left">
								<h4>Harga Rp. '.number_format($harga,2,",",".").'</h4>
							</div>
						</div>
						<div class="col-md-12">
							<p>'.$desc.'</p>
						</div>
					';
				}
			}
			else {
				$data = '';
			}
			echo json_encode(array(
				'data'=>$data
				));
		}
		else {
			echo json_encode(array(
				'data'=>$data
				));
		}
	}
}