<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Konfirmasi extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	}
	
	function index()
	{
		$this->load->view('html/header');
		$this->load->view('html/page/v_konfirmasi');
		$this->load->view('html/footer');
	}
}