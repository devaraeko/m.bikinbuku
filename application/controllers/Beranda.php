<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Beranda extends CI_Controller {

	function __construct()
	{
		parent::__construct();
        $this->load->library('user_agent');
		$this->load->library('m_db');
		
	}
	
	function index()
	{		
        $this->load->view('html/header');
        $this->load->view('html/page/home');
        $this->load->view('html/footer');
	}
	
}