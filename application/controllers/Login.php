<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('m_db');
		$this->load->model('login_model');
	}
	
	function index()
	{
		$username=$this->input->post('username');
		$password=$this->input->post('password');
			if($this->login_model->user_login($username,$password)==TRUE)
			{				
				if($this->login_model->cek_aktif($username,$password)==TRUE)
				{
					redirect(base_url());				
				}else{
					echo "<script>window.alert('Maaf, akun Anda belum aktif. Silakan aktifkan melalui email Anda!');window.location=('".base_url()."masuk')</script>";
				}				
			}else{
				echo "<script>window.alert('Maaf, username atau password salah. Silahkan coba lagi !');window.location=('".base_url()."masuk')</script>";
			}
				
	}
	
	function logout()
	{
		$this->login_model->user_logout();
	}

	function admin_login()
	{
		$this->form_validation->set_rules('username','Username','required');
		$this->form_validation->set_rules('password','Password','required');
		if($this->form_validation->run()==TRUE)
		{
			$username=$this->input->post('username',TRUE);
			$password=$this->input->post('password',TRUE);
			if($this->login_model->admin_login($username,$password)==TRUE)
			{				
				$masuk = $this->login_model->cek_aktif_admin($username,$password);
				if ($masuk['aktif']==TRUE)
				{
					redirect(base_url().'to_admin');					
				}
				else
				{
					echo "<script>window.alert('Maaf, akun admin Anda belum aktif. Silakan hubungi Master Admin bikinbuku.co.id!');window.location=('".base_url()."to_admin')</script>";
				}
							
			}else{
				echo "<script>window.alert('Maaf, username atau password salah. Silahkan coba lagi !');window.location=('".base_url()."to_admin')</script>";
			}
		}else{
            echo "<script>window.alert('Maaf, username atau password salah. Silahkan coba lagi !');window.location=('".base_url()."to_admin')</script>";
		}		
	}
}