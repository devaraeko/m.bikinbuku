<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class Error extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('m_db');
	}
	 function index()
	 {
	 	$meta['nav'] = "nothing";
		$meta['judul'] = baca_konfig('bikin-buku')." - Halaman Tidak Ditemukan";
		$meta['data']=$this->m_db->get_data('blog',array('banner'=>1));
		$this->load->view('html/header',$meta);
		$this->load->view('errors/v_error');
		$this->load->view('html/footer',$meta);
	 }
}