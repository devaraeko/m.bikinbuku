<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Unggah_naskah extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('m_db');
		$this->load->model('login_model');
		$this->load->model('naskah_model');
	}
	
	function index() {
		if(aktif()!=1)
		{
			$ref=$this->uri->ruri_string();
			redirect(base_url().'member/daftar?ref='.$ref);
		}
		else
		{
			$meta['nav'] = "layanan";
			$meta['judul'] = baca_konfig('bikin-buku')." - Unggah Naskah";
			$meta['data']=$this->m_db->get_data('blog',array('banner'=>1));
			$this->load->view('html/header',$meta);
			$this->load->view('html/page/v_unggah-naskah');
			$this->load->view('html/footer',$meta);
		}
	}

	function unggah() {
		$mbrid = $this->input->post('memberid');
		$judul = $this->input->post('judul');
		$pen = $this->input->post('penulis');
		$pnb = $this->input->post('penerbit');
		$kat = $this->input->post('ktgr');
		$kts = $this->input->post('kertas');
		$htm = $this->input->post('jum_hitam');
		$wrn = $this->input->post('jum_warna');
		$ctk = $this->input->post('jum_ctk');
		$hrg_ctk = $this->input->post('harga_cetak');
		$total_ctk = $this->input->post('harga_total');
		$hrg_ksm = $this->input->post('harga_konsumen');
		$pkt = $this->input->post('paket');
		$hrg_pkt = $this->input->post('harga_paket');
		$a1=$this->input->post('tulis');
		$a2=$this->input->post('sunting');
		$a3=$this->input->post('alih');
		$b1=$this->input->post('rancang');
		$b2=$this->input->post('tata');
		$b3=$this->input->post('ilus');
		$c1=$this->input->post('isbn');
		$c2=$this->input->post('cetak');
		$c3=$this->input->post('pasar');
		$sel_alih=$this->input->post('sel_alihbahasa');
		$cat = $this->input->post('catatan');

		$val = array(
			'kat_id'=>$kat
		);
		$nama_kat = $this->m_db->get_row('kat_buku',$val,'nama');
		$add = $this->naskah_model->naskah_add($mbrid,$judul,$pen,$pnb,$nama_kat,$kts,$htm,$wrn,$ctk,$hrg_ctk,$total_ctk,$hrg_ksm,$pkt,$hrg_pkt,$a1,$a2,$a3,$b1,$b2,$b3,$c1,$c2,$c3,$sel_alih,$cat);
		
		$pro_id = $add['projectID'];
		if ($add['status']==TRUE) {
			$add_biaya = $this->naskah_model->naskah_biaya($pro_id,$htm,$wrn,$ctk,$hrg_ctk,$total_ctk,$pkt,$hrg_pkt,$a1,$a2,$a3,$b1,$b2,$b3,$c1,$c2,$c3,$sel_alih);
			$pro_id = $add['projectID'];
			$config['upload_path'] = './assets/naskah/';
            $config['allowed_types'] = '*';
            $config['max_size'] = '1000000';

            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if(!empty($_FILES['naskah']['name'])){                
                if($this->upload->do_upload('naskah')){
                    $aa = $this->upload->data();
                    $naskah = $aa['file_name'];

                    	$config2['upload_path'] = './assets/cover/';
			            $config2['allowed_types'] = '*';
			            $config2['max_size'] = '1000000';
			            $this->load->library('upload', $config2);
            			$this->upload->initialize($config2);
            			$this->upload->do_upload('cover');
            			$bb = $this->upload->data();
                    	$cover = $bb['file_name'];
                    	if ($this->naskah_model->file_add($pro_id,$naskah,$cover)==TRUE) {
                    		redirect(base_url().'unggah_naskah/selesai/'.$pro_id);
                    	}
                    	
                   
                    
                }else{
                    echo $this->upload->display_errors();
                }
            }else{
                echo $this->upload->display_errors();
            }	
			
		}
		else
		{
			redirect(base_url().'unggah_naskah');
		}
	}

	function selesai()
	{
		$id = $this->uri->segment(3);
		$memberID = member_info('id');
		$val1 = array(
			'id_project'=>$id,
			'member_id'=>$memberID
		);
		$val2 = array(
			'id_project'=>$id
		);
		$val3 = array(
			'id'=>$memberID
		);
		$meta['nav'] = "login";
		$meta['judul'] = baca_konfig('bikin-buku')." - Selesai Mengunggah";
		$meta['data']=$this->m_db->get_data('blog',array('banner'=>1));
		$this->load->view('html/header',$meta);
		$v['pro'] = $this->m_db->get_data('project',$val1);
		$v['detail'] = $this->m_db->get_data('project_detail',$val2);
		$v['biaya'] = $this->m_db->get_data('project_biaya',$val2);
		$v['dtl_pkt'] = $this->m_db->get_data('paket_custom',$val2);
		$v['hrg_op'] = $this->m_db->get_data('biaya_operasional',array('id_biaya'=>1));
		$v['member'] = $this->m_db->get_data('member',$val3);
		$this->load->view('html/page/v_unggah_selesai',$v);
		$this->load->view('html/footer');

	}

	function invoice_cetak()
	{
		$id = $this->uri->segment(3);
		$memberID = member_info('id');
		$val1 = array(
			'id_project'=>$id,
			'member_id'=>$memberID
		);
		$val2 = array(
			'id_project'=>$id
		);
		$val3 = array(
			'id'=>$memberID
		);
		
		$v['pro'] = $this->m_db->get_data('project',$val1);
		$v['detail'] = $this->m_db->get_data('project_detail',$val2);
		$v['dtl_pkt'] = $this->m_db->get_data('paket_custom',$val2);
		$v['member'] = $this->m_db->get_data('member',$val3);
		$this->load->view('html/page/invoice_pdf',$v);
	}

	function tampilkertas()
	{
		$kertas=$this->uri->segment(3);
		$s=array(
		'id_kertas'=>$kertas
		);
		if($this->m_db->is_bof('kertas',$s)==FALSE)
		{
			$bw=$this->m_db->get_row('kertas',$s,'hitamputih');
			$wrn=$this->m_db->get_row('kertas',$s,'warna');
			echo json_encode(array(
			'hitamputih'=>$bw,
			'warna'=>$wrn,
			));
		}else{
			echo json_encode(array(
			'hitamputih'=>'Rp. 0',
			'warna'=>'Rp. 0'));
		}
	}

	function tampilpaket()
	{
		$pkt=$this->uri->segment(3);
		$s=array(
		'id_paket'=>$pkt
		);
		if($this->m_db->is_bof('paket',$s)==FALSE)
		{
			$hrg=$this->m_db->get_row('paket',$s,'harga');
			echo json_encode(array(
			'harga'=>$hrg
			));
		}else{
			echo json_encode(array(
			'harga'=>'Rp. 0'));
		}
	}

	function tampil_tagihan()
	{
		$id=$this->uri->segment(3);
		$s=array(
		'id_project'=>$id
		);
		if($this->m_db->is_bof('project_biaya',$s)==FALSE)
		{
			$hrg=$this->m_db->get_row('project_biaya',$s,'total_semua');
			echo json_encode(array(
			'harga'=>$hrg
			));
		}else{
			echo json_encode(array(
			'harga'=>'Rp. 0'));
		}
	}

	function hitung()
	{
		$hitam = $this->input->post('hitam');
		$warna = $this->input->post('warna');
		$jml_htm = $this->input->post('hrg_hitam');
		$jml_wrn = $this->input->post('hrg_warna');
		$cetak = $this->input->post('jml_cetak');
		$cov = $this->input->post('cover');
		$lam = $this->input->post('lami');
		$bin = $this->input->post('bind');
		$wrap = $this->input->post('wrapping');

		$total_htm = $hitam*$jml_htm;
		$total_wrn = $warna*$jml_wrn;
		$satu_prod = $total_htm+$total_wrn+$cov+$lam+$bin+$wrap;
		$total_prod = $cetak*$satu_prod;

		echo json_encode(array(
			'satu_produk'=>$satu_prod,
			'total_produk'=>$total_prod
		));
	}

	function status()
	{
		if(aktif()!=1)
		{
			$ref=$this->uri->ruri_string();
			redirect(base_url().'member/daftar?ref='.$ref);
		}
		else
		{
			$meta['nav'] = "layanan";
			$meta['judul'] = baca_konfig('bikin-buku')." - Status Pesanan";
			$meta['data']=$this->m_db->get_data('blog',array('banner'=>1));
			$this->load->view('html/header',$meta);
			$v['data'] = $this->naskah_model->naskah_data();
			$v['det'] = $this->m_db->get_data('project_biaya');
			$this->load->view('html/page/v_status_pesanan',$v);
			$this->load->view('html/footer',$meta);
		}
	}

	function tampildetail()
	{
		$id=$this->input->post('id_project');
		$s=array(
		'id_project'=>$id
		);
		if($this->m_db->is_bof('project',$s)==FALSE)
		{
			$id_pkt=$this->m_db->get_row('project',$s,'id_paket');
			$kd=$this->m_db->get_row('project',$s,'kode_order');
			$tgl=$this->m_db->get_row('project',$s,'tgl_order');
			$jdl=$this->m_db->get_row('project_detail',$s,'judul');
			$s1=array(
			'id_paket'=>$id_pkt
			);
			
			$dtl_paket=$this->m_db->get_data('paket_custom',$s);
			if ($id_pkt==10) {
				foreach ($dtl_paket as $r) {
					$a1 = $r->penulisan;
					if ($a1==1) {
						$b1 = "check";
					}
					else {
						$b1 = "close";
					}
					$a2 = $r->penyuntingan;
					if ($a2==1) {
						$b2 = "check";
					}
					else {
						$b2 = "close";
					}
					$a3 = $r->alihbahasa;
					if ($a3==1) {
						$b3 = "check";
					}
					else {
						$b3 = "close";
					}
					$a4 = $r->sampul;
					if ($a4==1) {
						$b4 = "check";
					}
					else {
						$b4 = "close";
					}
					$a5 = $r->tataletak;
					if ($a5==1) {
						$b5 = "check";
					}
					else {
						$b5 = "close";
					}
					$a6 = $r->ilustrasi;
					if ($a6==1) {
						$b6 = "check";
					}
					else {
						$b6 = "close";
					}
					$a7 = $r->isbn;
					if ($a7==1) {
						$b7 = "check";
					}
					else {
						$b7 = "close";
					}
					$a8 = $r->cetak;
					if ($a8==1) {
						$b8 = "check";
					}
					else {
						$b8 = "close";
					}
					$a9 = $r->pemasaran;
					if ($a9==1) {
						$b9 = "check";
					}
					else {
						$b9 = "close";
					}

					$nama_pkt="Sesuai kebutuhan";
					$dtl = '
						<div class="col-md-4">
							<ol class="rounded-list">
							    <li><a class="btn btn-'.$b1.' btn-circle"><i class="fa fa-'.$b1.'"></i></a>&nbsp;&nbsp;<a class="ket">PENULISAN</a></li>
							    <li><a class="btn btn-'.$b2.' btn-circle"><i class="fa fa-'.$b2.'"></i></a>&nbsp;&nbsp;<a class="ket">PENYUNTINGAN</a></li>
							    <li><a class="btn btn-'.$b3.' btn-circle"><i class="fa fa-'.$b3.'"></i></a>&nbsp;&nbsp;<a class="ket">ALIH BAHASA</a></li>                       
							</ol>
						</div>
						<div class="col-md-4">
							<ol class="rounded-list">
							    <li><a class="btn btn-'.$b4.' btn-circle"><i class="fa fa-'.$b4.'"></i></a>&nbsp;&nbsp;<a class="ket">RANCANG SAMPUL</a></li>
							    <li><a class="btn btn-'.$b5.' btn-circle"><i class="fa fa-'.$b5.'"></i></a>&nbsp;&nbsp;<a class="ket">TATA LETAK</a></li>
							    <li><a class="btn btn-'.$b6.' btn-circle"><i class="fa fa-'.$b6.'"></i></a>&nbsp;&nbsp;<a class="ket">ILUSTRASI</a></li>                       
							</ol>
						</div>
						<div class="col-md-4">
							<ol class="rounded-list">
							    <li><a class="btn btn-'.$b7.' btn-circle"><i class="fa fa-'.$b7.'"></i></a>&nbsp;&nbsp;<a class="ket">ISBN</a></li>
							    <li><a class="btn btn-'.$b8.' btn-circle"><i class="fa fa-'.$b8.'"></i></a>&nbsp;&nbsp;<a class="ket">CETAK</a></li>
							    <li><a class="btn btn-'.$b9.' btn-circle"><i class="fa fa-'.$b9.'"></i></a>&nbsp;&nbsp;<a class="ket">PEMASARAN</a></li>                       
							</ol>
						</div>
					';
				}
			}
			else {
				$nama_pkt=$this->m_db->get_row('paket',$s1,'nama_paket');
				$dtl = '';
			}
			
			echo json_encode(array(
			'judul'=>$jdl,
			'kode'=>$kd,
			'tgl'=>$tgl,
			'pkt'=>$nama_pkt,
			'dtl'=>$dtl
			));
		}else{
			echo json_encode(array(
			'judul'=>'-',
			'kode'=>'-',
			'tgl'=>'-',
			'pkt'=>'-',
			'dtl'=>''
			));
		}
	}

	function del_pesanan()
	{
		$IDnas = $this->input->get('id');
		if ($this->naskah_model->del_psn($IDnas)==TRUE) {
			set_header_message('success','Hapus Pesanan','Berhasil dihapus!');
			redirect(base_url('unggah_naskah/status'),'refresh',301);
		}
		else {
			set_header_message('danger','Hapus Pesanan','Gagal dihapus!');
			redirect(base_url('unggah_naskah/status'),'refresh',301);
		}
	}
}