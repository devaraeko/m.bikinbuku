<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class Kategori extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('m_db');		
		if(empty(admin_info('akses')))
		{
			redirect(base_url().'to_admin/login');
		}
		$this->load->model('konten_model');
	}

	function index()
	{
		$info['judul']="Kategori Tulisan/Blog";
		$this->load->view('admin/header',$info);
		$d['kat']=$this->m_db->get_data('blog_kategori');
		$this->load->view('admin/konten/v_kat',$d);
		$this->load->view('admin/footer');
	}

	function add_kat()
	{
		$nama=$this->input->post('nama');
		if ($this->konten_model->kat_tambah($nama)==TRUE) {
			set_header_message('success','Tambah Kategori','Berhasil menambahkan kategori');
			redirect(base_url('admin/konten/kategori'),'refresh',301);
		}
		else {
			set_header_message('success','Tambah Kategori','Gagal menambahkan kategori');
			redirect(base_url('admin/konten/kategori'),'refresh',301);
		}
	}

	function edit_kat()
	{
		$this->form_validation->set_rules('katID','ID Kategori','required');
		$this->form_validation->set_rules('nama','Nama Kategori','required');
		if($this->form_validation->run()==TRUE)
		{
			$IDkat=$this->input->post('katID',TRUE);
			$nama=$this->input->post('nama',TRUE);
			
			if($this->konten_model->kat_edit($IDkat,$nama)==TRUE)
			{
				set_header_message('success','Ubah Kategori','Berhasil mengubah kategori');
				redirect(base_url('admin/konten/kategori'),'refresh',301);
			}else{
				set_header_message('danger','Ubah Kategori','Gagal mengubah kategori');
				redirect(base_url('admin/konten/kategori'),'refresh',301);
			}		
		}else{
			$id=$this->input->get('id',TRUE);
			$info['judul']="Ubah Kategori Artikel/Tulisan";
			$this->load->view('admin/header',$info);
			$d['data']=$this->m_db->get_data('blog_kategori',array('id_kat_blog'=>$id));
			$this->load->view('admin/konten/v_kat_edit',$d);
			$this->load->view('admin/footer');
		}
	}

	function delete()
	{
		$id=$this->input->get('id',TRUE);
		if($this->konten_model->kat_hapus($id)==TRUE)
		{
			set_header_message('success','Hapus Kategori','Berhasil menghapus Kategori');
			redirect(base_url('admin/konten/kategori'),'refresh',301);
		}else{
			set_header_message('danger','Hapus Kategori','Gagal menghapus Kategori');
			redirect(base_url('admin/konten/kategori'),'refresh',301);
		}
	}
}