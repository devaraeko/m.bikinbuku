<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Artikel extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('m_db');		
		if(empty(admin_info('akses')))
		{
			redirect(base_url().'to_admin/login');
		}
		$this->load->model('konten_model');
	}

	function index()
	{
		$info['judul']="Semua Artikel/Blog";
		$this->load->view('admin/header',$info);
		$d['data']=$this->m_db->get_data('blog');
		$d['kat']=$this->m_db->get_data('blog_kategori');
		$this->load->view('admin/konten/v_artikel',$d);
		$this->load->view('admin/footer');
	}

	function add()
	{
		$this->form_validation->set_rules('judul','Judul Tulisan','required');
		$this->form_validation->set_rules('meta','Meta Tulisan','required');
		$this->form_validation->set_rules('kategori','Kategori Tulisan','required');
		$this->form_validation->set_rules('penulis','Nama Penulis','required');
		$this->form_validation->set_rules('isi','Isi Tulisan','required');
		if($this->form_validation->run()==TRUE)
		{
			$jdl=$this->input->post('judul',TRUE);
			$met=$this->input->post('meta',TRUE);
			$kat=$this->input->post('kategori',TRUE);
			$pen=$this->input->post('penulis',TRUE);
			$isi=$this->input->post('isi',TRUE);
			$cover='upload';
			
			if($this->konten_model->artikel_add($jdl,$met,$kat,$pen,$isi,$cover)==TRUE)
			{
				set_header_message('success','Tambah Artikel','Berhasil menambahkan artikel');
				redirect(base_url('admin/konten/artikel'),'refresh',301);
			}else{
				set_header_message('danger','Tambah Artikel','Gagal menambahkan artikel');
				redirect(base_url('admin/konten/artikel'),'refresh',301);
			}			
		}else{
			$info['judul']="Tambah Artikel/Tulisan";
			$this->load->view('admin/header',$info);
			$this->load->view('admin/konten/v_artikel_tambah');
			$this->load->view('admin/footer');
		}
	}

	function getkat()
	{
		$tipe=$this->input->get('tipe');
		if(!empty($tipe))
		{
			$tb=$tipe;
			$d=$this->m_db->get_data($tb,'','nama_kat ASC');
			echo json_encode($d);
		}else{
			echo json_encode(array());
		}
	}

	function edit()
	{
		$this->form_validation->set_rules('blogID','ID Artikel','required');
		$this->form_validation->set_rules('judul','Judul Tulisan','required');
		$this->form_validation->set_rules('meta','Meta Tulisan','required');
		$this->form_validation->set_rules('kategori','Kategori Tulisan','required');
		$this->form_validation->set_rules('penulis','Nama Penulis','required');
		$this->form_validation->set_rules('isi','Isi Tulisan','required');
		if($this->form_validation->run()==TRUE)
		{
			$IDblog=$this->input->post('blogID',TRUE);
			$jdl=$this->input->post('judul',TRUE);
			$met=$this->input->post('meta',TRUE);
			$kat=$this->input->post('kategori',TRUE);
			$pen=$this->input->post('penulis',TRUE);
			$isi=$this->input->post('isi',TRUE);
			$cover='upload';
			
			if($this->konten_model->artikel_edit($IDblog,$jdl,$met,$kat,$pen,$isi,$cover)==TRUE)
			{
				set_header_message('success','Ubah Isi Artikel','Berhasil mengubah artikel');
				redirect(base_url('admin/konten/artikel'),'refresh',301);
			}else{
				set_header_message('danger','Ubah Isi Artikel','Gagal mengubah artikel');
				redirect(base_url('admin/konten/artikel'),'refresh',301);
			}		
		}else{
			$id=$this->input->get('id',TRUE);
			$info['judul']="Edit Artikel/Tulisan";
			$this->load->view('admin/header',$info);
			$d['data']=$this->m_db->get_data('blog',array('id_blog'=>$id));
			$this->load->view('admin/konten/v_artikel_edit',$d);
			$this->load->view('admin/footer');
		}
	}

	function delete()
	{
		$id=$this->input->get('id',TRUE);
		if($this->konten_model->artikel_delete($id)==TRUE)
		{
			set_header_message('success','Hapus Artikel','Berhasil menghapus artikel/tulisan');
			redirect(base_url('admin/konten/artikel'),'refresh',301);
		}else{
			set_header_message('danger','Hapus Artikel','Gagal menghapus artikel/tulisan');
			redirect(base_url('admin/konten/artikel'),'refresh',301);
		}
	}

	function add_kat()
	{
		$nama=$this->input->post('nama');
		if ($this->konten_model->kat_tambah($nama)==TRUE) {
			set_header_message('success','Tambah Kategori','Berhasil menambahkan kategori');
			redirect(base_url('admin/konten/artikel'),'refresh',301);
		}
		else {
			set_header_message('success','Tambah Kategori','Gagal menambahkan kategori');
			redirect(base_url('admin/konten/artikel'),'refresh',301);
		}
	}

	function setbanner()
	{
		$id=$this->input->get('id',TRUE);
		if ($this->konten_model->set($id)==TRUE) {
			set_header_message('success','Set Banner','Berhasil memasang artikel pada banner');
			redirect(base_url('admin/konten/artikel'),'refresh',301);
		}else{
			set_header_message('danger','Set Banner','Gagal memasang artikel pada banner');
			redirect(base_url('admin/konten/artikel'),'refresh',301);
		}
	}

	function unsetbanner()
	{
		$id=$this->input->get('id',TRUE);
		if ($this->konten_model->un_set($id)==TRUE) {
			set_header_message('success','Unset Banner','Berhasil mencopot artikel pada banner');
			redirect(base_url('admin/konten/artikel'),'refresh',301);
		}else{
			set_header_message('danger','Unset Banner','Gagal mencopot artikel pada banner');
			redirect(base_url('admin/konten/artikel'),'refresh',301);
		}
	}

	function getdata()
	{
		$tipe=$this->input->get('tipe');
		if(!empty($tipe))
		{
			$tbl=$tipe;
			$d=$this->m_db->get_data($tbl);
			echo json_encode($d);
		}else{
			echo json_encode(array());
		}
	}

	function sematkan()
	{
		$IDblog = $this->input->post('blogID');
		$IDsemat = field_value('blog','sematkan',1,'id_blog');
		if ($this->konten_model->semat($IDblog,$IDsemat)==TRUE) {
			set_header_message('success','Sematkan Artikel','Berhasil menyematkan artikel');
			redirect(base_url('admin/konten/artikel'),'refresh',301);
		}
		else {
			set_header_message('danger','Sematkan Artikel','Gagal menyematkan artikel');
			redirect(base_url('admin/konten/artikel'),'refresh',301);
		}
	}
}