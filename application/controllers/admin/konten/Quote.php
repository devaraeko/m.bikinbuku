<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Quote extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('m_db');		
		if(empty(admin_info('akses')))
		{
			redirect(base_url().'to_admin/login');
		}
		$this->load->model('konten_model');
	}

	function index()
	{
		$info['judul']="Quote Website";
		$this->load->view('admin/header',$info);
		$d['data']=$this->m_db->get_data('quote');
		$this->load->view('admin/konten/v_quote',$d);
		$this->load->view('admin/footer');
	}

	function add()
	{
		$this->form_validation->set_rules('nama','Nama','required');
		$this->form_validation->set_rules('quote','Isi Quote','required');
		if($this->form_validation->run()==TRUE)
		{
			$nama=$this->input->post('nama',TRUE);
			$quo=$this->input->post('quote',TRUE);
			
			if($this->konten_model->quote_add($nama,$quo)==TRUE)
			{
				set_header_message('success','Tambah Quote Website','Berhasil menambahkan quote');
				redirect(base_url('admin/konten/quote'),'refresh',301);
			}else{
				set_header_message('danger','Tambah Quote Website','Gagal menambahkan quote');
				redirect(base_url('admin/konten/quote'),'refresh',301);
			}			
		}else{
			$info['judul']="Tambah Quote Website";
			$this->load->view('admin/header',$info);
			$this->load->view('admin/konten/v_quote_tambah');
			$this->load->view('admin/footer');
		}
	}

	function edit()
	{
		$this->form_validation->set_rules('quoteID','ID Quote','required');
		$this->form_validation->set_rules('nama','Nama','required');
		$this->form_validation->set_rules('quote','Isi Quote','required');
		if($this->form_validation->run()==TRUE)
		{
			$IDquote=$this->input->post('quoteID',TRUE);
			$nama=$this->input->post('nama',TRUE);
			$quo=$this->input->post('quote',TRUE);
			
			if($this->konten_model->quote_edit($IDquote,$nama,$quo)==TRUE)
			{
				set_header_message('success','Tambah Quote Website','Berhasil menambahkan quote');
				redirect(base_url('admin/konten/quote'),'refresh',301);
			}else{
				set_header_message('danger','Tambah Quote Website','Gagal menambahkan quote');
				redirect(base_url('admin/konten/quote'),'refresh',301);
			}			
		}else{
			$id=$this->input->get('id',TRUE);
			$info['judul']="Edit Quote Website";
			$this->load->view('admin/header',$info);
			$d['data']=$this->m_db->get_data('quote',array('id_quote'=>$id));
			$this->load->view('admin/konten/v_quote_edit',$d);
			$this->load->view('admin/footer');
		}
	}

	function delete()
	{
		$id=$this->input->get('id',TRUE);
		if($this->konten_model->quote_delete($id)==TRUE)
		{
			set_header_message('success','Hapus Quote','Berhasil menghapus quote');
			redirect(base_url('admin/konten/quote'),'refresh',301);
		}else{
			set_header_message('danger','Hapus Quote','Gagal menghapus quote');
			redirect(base_url('admin/konten/quote'),'refresh',301);
		}
	}
}