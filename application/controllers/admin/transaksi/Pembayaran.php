<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pembayaran extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('m_db');		
		if(empty(admin_info('akses')))
		{
			redirect(base_url().'to_admin/login');
		}
		$this->load->model('naskah_model');
	}

	function index()
	{
		$info['judul']="Pembayaran Masuk";
		$this->load->view('admin/header',$info);
		$d['data']=$this->naskah_model->get_data();
		$this->load->view('admin/transaksi/v_pembayaran',$d);
		$this->load->view('admin/footer');
	}

	function add()
	{

	}


}