<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Naskah extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('m_db');		
		if(empty(admin_info('akses')))
		{
			redirect(base_url().'to_admin/login');
		}
		$this->load->model('naskah_model');
	}

	function index()
	{
		$info['judul']="Naskah Masuk";
		$this->load->view('admin/header',$info);
		$d['data']=$this->naskah_model->get_data();
		$this->load->view('admin/transaksi/v_project',$d);
		$this->load->view('admin/footer');
	}

	function add()
	{

	}

	function ubah_proses()
	{
		$id_pro = $this->input->get('id');
		if ($this->naskah_model->ubah_status_proses($id_pro)==TRUE) {
			set_header_message('success','Ubah Status Project','Berhasil mengubah status project');
				redirect(base_url('admin/transaksi/naskah'),'refresh',301);
		}
		else {
			set_header_message('danger','Ubah Status Project','Gagal mengubah status project');
				redirect(base_url('admin/transaksi/naskah'),'refresh',301);
		}
	}

	function ubah_selesai()
	{
		$id_pro = $this->input->get('id');
		if ($this->naskah_model->ubah_status_selesai($id_pro)==TRUE) {
			set_header_message('success','Ubah Status Project','Berhasil mengubah status project');
				redirect(base_url('admin/transaksi/naskah'),'refresh',301);
		}
		else {
			set_header_message('danger','Ubah Status Project','Gagal mengubah status project');
				redirect(base_url('admin/transaksi/naskah'),'refresh',301);
		}
	}


}