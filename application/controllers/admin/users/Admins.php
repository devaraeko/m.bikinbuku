<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class Admins extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('m_db');		
		if(empty(admin_info('akses')))
		{
			redirect(base_url().'to_admin/login');
		}
		$this->load->model('konten_model');
	}
}