<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class Member extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('m_db');		
		if(empty(admin_info('akses')))
		{
			redirect(base_url().'to_admin/login');
		}
		$this->load->model('konten_model');
	}

	function index()
	{
		$info['judul']="Daftar Member";
		$this->load->view('admin/header',$info);
		$d['data']=$this->m_db->get_data('member');
		$this->load->view('admin/users/v_member',$d);
		$this->load->view('admin/footer');
	}
}