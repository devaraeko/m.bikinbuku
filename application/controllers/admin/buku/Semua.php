<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Semua extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('m_db');		
		if(empty(admin_info('akses')))
		{
			redirect(base_url().'to_admin/login');
		}
		$this->load->model('buku_model');
	}
	
	function index()
	{
		$info['judul']="Semua Buku";
		$this->load->view('admin/header',$info);
		$d['data']=$this->buku_model->buku_data();
		$this->load->view('admin/buku/v_buku',$d);
		$this->load->view('admin/footer');
		
	}

	function getdata()
	{
		$tipe=$this->input->get('tipe');
		if(!empty($tipe))
		{
			$tb=$tipe;
			if($tipe=="kategori")
			{
				$tb="kat_buku";
			}
			$d=$this->m_db->get_data($tb,'','nama ASC');
			echo json_encode($d);
		}else{
			echo json_encode(array());
		}
	}

	function add()
	{
		$this->form_validation->set_rules('judul','Judul Buku','required');
		$this->form_validation->set_rules('penulis','Penulis Buku','required');
		$this->form_validation->set_rules('penerbit','Penerbit Buku','required');
		$this->form_validation->set_rules('kategori','Kategori Buku','required');
		$this->form_validation->set_rules('isbn','ISBN','required');
		$this->form_validation->set_rules('terbit','Tanggal Terbit','required');
		$this->form_validation->set_rules('harga','Harga Buku','required');
		$this->form_validation->set_rules('ukuran','Ukuran Buku','required');
		$this->form_validation->set_rules('halaman','Halaman Buku','required');
		$this->form_validation->set_rules('sinopsis','Sinopsis Buku','required');
		if($this->form_validation->run()==TRUE)
		{
			$jdl=$this->input->post('judul',TRUE);
			$penu=$this->input->post('penulis',TRUE);
			$pener=$this->input->post('penerbit',TRUE);
			$kat=$this->input->post('kategori',TRUE);
			$isbn=$this->input->post('isbn',TRUE);
			$tbt=$this->input->post('terbit',TRUE);
			$hrg=$this->input->post('harga',TRUE);
			$uku=$this->input->post('ukuran',TRUE);
			$hal=$this->input->post('halaman',TRUE);
			$sin=$this->input->post('sinopsis',TRUE);
			$photo='upload';
			if($this->buku_model->buku_tambah($kat,$jdl,$penu,$pener,$isbn,$tbt,$hrg,$uku,$hal,$sin,$photo)==TRUE)
			{
				$bukuID=$this->m_db->last_insert_id();
				set_header_message('success','Tambah Buku','Berhasil menambah buku');
				redirect(base_url('admin/buku/semua'),'refresh',301);
			}else{
				set_header_message('danger','Tambah Produk','Gagal menambah produk');
				redirect(base_url('/admin/buku/semua/add'),'refresh',301);
			}
		}else{
			$meta['judul']="Tambah Buku";
			$this->load->view('admin/header',$meta);
			$this->load->view('admin/buku/v_buku_tambah');
			$this->load->view('admin/footer');
		}		
	}

	function edit()
	{
		$this->form_validation->set_rules('bukuid','ID Buku','required');
		$this->form_validation->set_rules('judul','Judul Buku','required');
		$this->form_validation->set_rules('penulis','Penulis Buku','required');
		$this->form_validation->set_rules('penerbit','Penerbit Buku','required');
		$this->form_validation->set_rules('kategori','Kategori Buku','required');
		$this->form_validation->set_rules('isbn','ISBN','required');
		$this->form_validation->set_rules('terbit','Tanggal Terbit','required');
		$this->form_validation->set_rules('harga','Harga Buku','required');
		$this->form_validation->set_rules('ukuran','Ukuran Buku','required');
		$this->form_validation->set_rules('halaman','Halaman Buku','required');
		$this->form_validation->set_rules('sinopsis','Sinopsis Buku','required');
		if($this->form_validation->run()==TRUE)
		{
			$bukuID=$this->input->post('bukuid',TRUE);
			$jdl=$this->input->post('judul',TRUE);
			$penu=$this->input->post('penulis',TRUE);
			$pener=$this->input->post('penerbit',TRUE);
			$kat=$this->input->post('kategori',TRUE);
			$isbn=$this->input->post('isbn',TRUE);
			$tbt=$this->input->post('terbit',TRUE);
			$hrg=$this->input->post('harga',TRUE);
			$uku=$this->input->post('ukuran',TRUE);
			$hal=$this->input->post('halaman',TRUE);
			$sin=$this->input->post('sinopsis',TRUE);
			$photo='upload';
			if($this->buku_model->buku_edit($bukuID,$kat,$jdl,$penu,$pener,$isbn,$tbt,$hrg,$uku,$hal,$sin,$photo)==TRUE)
			{
				set_header_message('success','Edit Buku','Berhasil mengedit buku');
				redirect(base_url('admin/buku/semua'),'refresh',301);
			}else{
				set_header_message('danger','Edit Produk','Gagal mengedit produk');
				redirect(base_url('/admin/buku/semua'),'refresh',301);
			}
		}else{
			$id=$this->input->get('id',TRUE);
			$meta['judul']="Edit Buku";
			$d['data']=$this->buku_model->buku_data(array('id_buku'=>$id));
			$this->load->view('admin/header',$meta);
			$this->load->view('admin/buku/v_buku_edit',$d);
			$this->load->view('admin/footer');
		}		
	}

	function delete()
	{
		$bukuID=$this->input->get('id',TRUE);
		if($this->buku_model->buku_delete($bukuID)==TRUE)
		{			
			set_header_message('success','Hapus Buku','Berhasil menghapus buku');
			redirect(base_url('admin/buku/semua'),'refresh',301);
		}else{
			set_header_message('danger','Hapus Buku','Gagal menghapus buku');
			redirect(base_url('admin/buku/semua'),'refresh',301);
		}
	}
}