<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kategori extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('m_db');		
		if(empty(admin_info('akses')))
		{
			redirect(base_url().'to_admin/login');
		}
		$this->load->model('buku_model');
	}
	
	function index()
	{		
		$info['judul']="Kategori Buku";
		$this->load->view('admin/header',$info);
		$d['data']=$this->buku_model->kategori_data();
		$this->load->view('admin/buku/v_kategori',$d);
		$this->load->view('admin/footer');
	}
	
	function add()
	{
		$this->form_validation->set_rules('nama','Nama Kategori Buku','required');
		if($this->form_validation->run()==TRUE)
		{
			$nama=$this->input->post('nama',TRUE);
			
			if($this->buku_model->kategori_add($nama)==TRUE)
			{
				set_header_message('success','Tambah Kategori Buku','Berhasil menambahkan Kategori Buku');
				redirect(base_url('admin/buku/kategori'),'refresh',301);
			}else{
				set_header_message('danger','Tambah Kategori Buku','Gagal menambahkan Kategori Buku');
				redirect(base_url('admin/buku/kategori'),'refresh',301);
			}			
		}else{
			redirect(base_url('admin/buku/kategori'),'refresh',301);
		}
	}
	
	function edit()
	{
		$this->form_validation->set_rules('katID','ID Kategori Buku','required');
		$this->form_validation->set_rules('nama','Nama Kategori Buku','required');
		if($this->form_validation->run()==TRUE)
		{
			$katID=$this->input->post('katID',TRUE);
			$nama=$this->input->post('nama',TRUE);
			
			if($this->buku_model->kategori_edit($katID,$nama)==TRUE)
			{
				set_header_message('success','Ubah Kategori Buku','Berhasil mengubah Kategori Buku');
				redirect(base_url('admin/buku/kategori'),'refresh',301);
			}else{
				set_header_message('danger','Ubah Kategori Buku','Gagal mengubah Kategori Buku');
				redirect(base_url('admin/buku/kategori'),'refresh',301);
			}			
		}else{
			$id=$this->input->get('id',TRUE);
			$info['judul']="Ubah Kategori Buku";
			$this->load->view('admin/header',$info);
			$d['data']=$this->m_db->get_data('kat_buku',array('kat_id'=>$id));
			$this->load->view('admin/buku/v_kategori_edit',$d);
			$this->load->view('admin/footer');
		}
	}
	
	function delete()
	{
		$id=$this->input->get('id',TRUE);
		if($this->buku_model->kategori_delete($id)==TRUE)
		{
			set_header_message('success','Hapus Kategori Buku','Berhasil menghapus Kategori Buku');
			redirect(base_url('admin/buku/kategori'),'refresh',301);
		}else{
			set_header_message('danger','Hapus Kategori Buku','Gagal menghapus Kategori Buku');
			redirect(base_url('admin/buku/kategori'),'refresh',301);
		}
	}
}