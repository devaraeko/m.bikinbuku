<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class To_admin extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('m_db');
		$this->load->model('login_model');
	}
	
	function index()
	{
		if(empty(admin_info('akses')))
		{
			redirect(base_url().'to_admin/login');
		}
		else
		{
			$akses = admin_info('akses');
			$info['akses'] = $akses;
			$info['judul']="Dashboard Operator";
			$this->load->view('admin/header',$info);
			$this->load->view('admin/dashboard',$info);
			$this->load->view('admin/footer');
		}
		
	}

	function login()
	{
		$this->load->view('admin/login');
	}

	function logout()
	{
		$this->login_model->admin_logout();
	}
}