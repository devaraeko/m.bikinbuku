<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Faq extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('m_db');		
		if(empty(admin_info('akses')))
		{
			redirect(base_url().'to_admin/login');
		}
		$this->load->model('konfig_model');
	}

	function index()
	{
		$info['judul']="Frequently Asked Questions";
		$this->load->view('admin/header',$info);
		$d['data']=$this->m_db->get_data('faq');
		$this->load->view('admin/konfig/v_faq',$d);
		$this->load->view('admin/footer');
	}

	function add()
	{
		$this->form_validation->set_rules('tanya','Pertanyaan','required');
		$this->form_validation->set_rules('jawab','Jawaban','required');
		if($this->form_validation->run()==TRUE)
		{
			$tny=$this->input->post('tanya',TRUE);
			$jwb=$this->input->post('jawab',TRUE);
			
			if($this->konfig_model->faq_add($tny,$jwb)==TRUE)
			{
				set_header_message('success','Tambah Konten FAQ','Berhasil menambahkan data FAQ');
				redirect(base_url('admin/konfig/faq'),'refresh',301);
			}else{
				set_header_message('danger','Tambah Konten FAQ','Gagal menambahkan data FAQ');
				redirect(base_url('admin/konfig/faq'),'refresh',301);
			}			
		}else{
			$info['judul']="Tambah Data FAQ";
			$this->load->view('admin/header',$info);
			$this->load->view('admin/konfig/v_faq_tambah');
			$this->load->view('admin/footer');
		}
	}

	function edit()
	{
		$this->form_validation->set_rules('IDfaq','FAQ ID','required');
		$this->form_validation->set_rules('tanya','Pertanyaan','required');
		$this->form_validation->set_rules('jawab','Jawaban','required');
		if($this->form_validation->run()==TRUE)
		{
			$faqID=$this->input->post('IDfaq',TRUE);
			$tny=$this->input->post('tanya',TRUE);
			$jwb=$this->input->post('jawab',TRUE);
			
			if($this->konfig_model->faq_edit($faqID,$tny,$jwb)==TRUE)
			{
				set_header_message('success','Ubah Konten FAQ','Berhasil mengubah data FAQ');
				redirect(base_url('admin/konfig/faq'),'refresh',301);
			}else{
				set_header_message('danger','Ubah Konten FAQ','Gagal mengubah data FAQ');
				redirect(base_url('admin/konfig/faq'),'refresh',301);
			}			
		}else{
			$id = $this->input->get('id');
			$info['judul']="Ubah Data FAQ";
			$this->load->view('admin/header',$info);
			$d['data']=$this->m_db->get_data('faq',array('id_faq'=>$id));
			$this->load->view('admin/konfig/v_faq_edit',$d);
			$this->load->view('admin/footer');
		}
	}

	function delete()
	{
		$id=$this->input->get('id',TRUE);
		if($this->konfig_model->faq_delete($id)==TRUE)
		{
			set_header_message('success','Hapus Data FAQ','Berhasil menghapus data FAQ');
			redirect(base_url('admin/konfig/faq'),'refresh',301);
		}else{
			set_header_message('danger','Hapus Data FAQ','Gagal menghapus data FAQ');
			redirect(base_url('admin/konfig/faq'),'refresh',301);
		}
	}
}