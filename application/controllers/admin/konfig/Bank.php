<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class Bank extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('m_db');		
		if(empty(admin_info('akses')))
		{
			redirect(base_url().'to_admin/login');
		}
		$this->load->model('konfig_model');
	}

	function index()
	{
		$info['judul']="Akun Bank Bikinbuku";
		$this->load->view('admin/header',$info);
		$d['data']=$this->m_db->get_data('bank');
		$this->load->view('admin/konfig/v_bank',$d);
		$this->load->view('admin/footer');
	}

	function add()
	{
		$this->form_validation->set_rules('n_bank','Nama Bank','required');
		$this->form_validation->set_rules('ats_nama','Atas Nama','required');
		$this->form_validation->set_rules('no_rek','Nomor Rekening','required');
		if($this->form_validation->run()==TRUE)
		{
			$nama=$this->input->post('n_bank',TRUE);
			$pem=$this->input->post('ats_nama',TRUE);
			$norek=$this->input->post('no_rek',TRUE);
			
			if($this->konfig_model->bank_add($nama,$pem,$norek)==TRUE)
			{
				set_header_message('success','Tambah Akun Bank','Berhasil menambahkan akun bank');
				redirect(base_url('admin/konfig/bank'),'refresh',301);
			}else{
				set_header_message('danger','Tambah Akun Bank','Gagal menambahkan akun bank');
				redirect(base_url('admin/konfig/bank'),'refresh',301);
			}			
		}else{
			redirect(base_url('admin/konfig/bank'),'refresh',301);
		}
	}

	function edit()
	{
		$this->form_validation->set_rules('bankID','ID Jenis Paket','required');
		$this->form_validation->set_rules('n_bank','Nama Bank','required');
		$this->form_validation->set_rules('ats_nama','Atas Nama','required');
		$this->form_validation->set_rules('no_rek','Nomor Rekening','required');
		if($this->form_validation->run()==TRUE)
		{
			$bankID=$this->input->post('bankID',TRUE);
			$nama=$this->input->post('n_bank',TRUE);
			$pem=$this->input->post('ats_nama',TRUE);
			$norek=$this->input->post('no_rek',TRUE);
			
			if($this->konfig_model->bank_edit($bankID,$nama,$pem,$norek)==TRUE)
			{
				set_header_message('success','Ubah Akun Bank','Berhasil mengubah akun bank');
				redirect(base_url('admin/konfig/bank'),'refresh',301);
			}else{
				set_header_message('danger','Ubah Akun Bank','Gagal mengubah akun bank');
				redirect(base_url('admin/konfig/bank'),'refresh',301);
			}			
		}else{
			$id=$this->input->get('id',TRUE);
			$info['judul']="Edit Data Akun Bank";
			$this->load->view('admin/header',$info);
			$d['data']=$this->m_db->get_data('bank',array('bank_id'=>$id));
			$this->load->view('admin/konfig/v_bank_edit',$d);
			$this->load->view('admin/footer');
		}
	}

	function delete()
	{
		$id=$this->input->get('id',TRUE);
		if($this->konfig_model->bank_delete($id)==TRUE)
		{
			set_header_message('success','Hapus Akun Bank','Berhasil menghapus akun bank');
			redirect(base_url('admin/konfig/bank'),'refresh',301);
		}else{
			set_header_message('danger','Hapus Akun Bank','Gagal menghapus akun bank');
			redirect(base_url('admin/konfig/bank'),'refresh',301);
		}
	}
}