<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class Operasional extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('m_db');		
		if(empty(admin_info('akses')))
		{
			redirect(base_url().'to_admin/login');
		}
		$this->load->model('konfig_model');
	}

	function index()
	{
		$info['judul']="Biaya Operasional";
		$this->load->view('admin/header',$info);
		$id = 1;
		$d['data']=$this->m_db->get_data('biaya_operasional',array('id_biaya'=>$id));
		$this->load->view('admin/konfig/v_operasional',$d);
		$this->load->view('admin/footer');
	}

	function edit()
	{
		$ID_op=$this->input->post('ID_op');
		$tls=$this->input->post('tulis');
		$sun=$this->input->post('sunting');
		$indo=$this->input->post('in');
		$eng=$this->input->post('eng');
		$rcg=$this->input->post('rancang');
		$tata=$this->input->post('tata');
		$ilus=$this->input->post('ilus');
		$isbn=$this->input->post('isbn');
		$psr=$this->input->post('pasar');

		if($this->konfig_model->biaya_edit($ID_op,$tls,$sun,$indo,$eng,$rcg,$tata,$ilus,$isbn,$psr)==TRUE)
		{
			set_header_message('success','Ubah Biaya Operasional','Berhasil mengubah data');
			redirect(base_url('admin/konfig/operasional'),'refresh',301);
		}else{
			set_header_message('danger','Ubah Biaya Operasional','Gagal mengubah data');
			redirect(base_url('admin/konfig/operasional'),'refresh',301);
		}
	}
}