<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kertas extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('m_db');		
		if(empty(admin_info('akses')))
		{
			redirect(base_url().'to_admin/login');
		}
		$this->load->model('konfig_model');
	}

	function index()
	{
		$info['judul']="Jenis Kertas";
		$this->load->view('admin/header',$info);
		$d['data']=$this->m_db->get_data('kertas');
		$this->load->view('admin/konfig/v_kertas',$d);
		$this->load->view('admin/footer');
	}

	function add()
	{
		$this->form_validation->set_rules('nama','Nama Jenis Kertas','required');
		$this->form_validation->set_rules('htmpth','Harga Hitam Putih','required');
		$this->form_validation->set_rules('warna','Harga Warna','required');
		if($this->form_validation->run()==TRUE)
		{
			$nama=$this->input->post('nama',TRUE);
			$htm=$this->input->post('htmpth',TRUE);
			$wrn=$this->input->post('warna',TRUE);
			
			if($this->konfig_model->kertas_add($nama,$htm,$wrn)==TRUE)
			{
				set_header_message('success','Tambah Jenis Kertas','Berhasil menambahkan Jenis Kertas');
				redirect(base_url('admin/konfig/kertas'),'refresh',301);
			}else{
				set_header_message('danger','Tambah Jenis Kertas','Gagal menambahkan Jenis Kertas');
				redirect(base_url('admin/konfig/kertas'),'refresh',301);
			}			
		}else{
			redirect(base_url('admin/konfig/kertas'),'refresh',301);
		}
	}

	function edit()
	{
		$this->form_validation->set_rules('kertasID','ID Jenis Kertas','required');
		$this->form_validation->set_rules('nama','Nama Jenis Kertas','required');
		$this->form_validation->set_rules('hitam','Harga Hitam Putih','required');
		$this->form_validation->set_rules('warna','Harga Warna','required');
		if($this->form_validation->run()==TRUE)
		{
			$ktsID=$this->input->post('kertasID',TRUE);
			$nama=$this->input->post('nama',TRUE);
			$htm=$this->input->post('hitam',TRUE);
			$wrn=$this->input->post('warna',TRUE);
			
			if($this->konfig_model->kertas_edit($ktsID,$nama,$htm,$wrn)==TRUE)
			{
				set_header_message('success','Ubah Jenis Kertas','Berhasil mengubah Jenis Kertas');
				redirect(base_url('admin/konfig/kertas'),'refresh',301);
			}else{
				set_header_message('danger','Ubah Jenis Kertas','Gagal mengubah Jenis Kertas');
				redirect(base_url('admin/konfig/kertas'),'refresh',301);
			}			
		}else{
			$id=$this->input->get('id',TRUE);
			$info['judul']="Edit Jenis Kertas";
			$this->load->view('admin/header',$info);
			$d['data']=$this->m_db->get_data('kertas',array('id_kertas'=>$id));
			$this->load->view('admin/konfig/v_kertas_edit',$d);
			$this->load->view('admin/footer');
		}
	}

	function delete()
	{
		$id=$this->input->get('id',TRUE);
		if($this->konfig_model->kertas_delete($id)==TRUE)
		{
			set_header_message('success','Hapus Jenis Kertas','Berhasil menghapus Jenis Kertas');
			redirect(base_url('admin/konfig/kertas'),'refresh',301);
		}else{
			set_header_message('danger','Hapus Jenis Kertas','Gagal menghapus Jenis Kertas');
			redirect(base_url('admin/konfig/kertas'),'refresh',301);
		}
	}


}