<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class Syarat extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('m_db');		
		if(empty(admin_info('akses')))
		{
			redirect(base_url().'to_admin/login');
		}
		$this->load->model('konfig_model');
	}

	function index()
	{
		$info['judul']="Persyaratan";
		$id = 8;
		$this->load->view('admin/header',$info);
		$d['data']=$this->m_db->get_data('konfigurasi',array('konfigurasi_id'=>$id));
		$this->load->view('admin/konfig/v_syarat',$d);
		$this->load->view('admin/footer');
	}

	function edit()
	{
		$this->form_validation->set_rules('konfigID','ID Konfigurasi','required');
		$this->form_validation->set_rules('isi','Isi Konfigurasi','required');
		if($this->form_validation->run()==TRUE)
		{
			$konfID=$this->input->post('konfigID',TRUE);
			$isi=$this->input->post('isi',TRUE);
			
			if($this->konfig_model->syarat_edit($konfID,$isi)==TRUE)
			{
				set_header_message('success','Ubah Persyaratan','Berhasil mengubah Persyaratan');
				redirect(base_url('admin/konfig/syarat'),'refresh',301);
			}else{
				set_header_message('danger','Ubah Persyaratan','Gagal mengubah Persyaratan');
				redirect(base_url('admin/konfig/syarat'),'refresh',301);
			}			
		}else{
			redirect(base_url().'admin/konfig/syarat');
		}
	}
}