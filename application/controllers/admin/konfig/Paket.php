<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Paket extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('m_db');		
		if(empty(admin_info('akses')))
		{
			redirect(base_url().'to_admin/login');
		}
		$this->load->model('konfig_model');
	}

	function index()
	{
		$info['judul']="Paket Penerbitan";
		$this->load->view('admin/header',$info);
		$d['data']=$this->m_db->get_data('paket');
		$this->load->view('admin/konfig/v_paket',$d);
		$this->load->view('admin/footer');
	}

	function add()
	{
		$this->form_validation->set_rules('nama','Nama Paket Penerbitan','required');
		$this->form_validation->set_rules('harga','Harga Paket','required');
		$this->form_validation->set_rules('detail','Detail Paket','required');
		if($this->form_validation->run()==TRUE)
		{
			$nama=$this->input->post('nama',TRUE);
			$harga=$this->input->post('harga',TRUE);
			$det=$this->input->post('detail',TRUE);
			
			if($this->konfig_model->paket_add($nama,$harga,$det)==TRUE)
			{
				set_header_message('success','Tambah Jenis Paket','Berhasil menambahkan Paket Penerbitan');
				redirect(base_url('admin/konfig/paket'),'refresh',301);
			}else{
				set_header_message('danger','Tambah Jenis Paket','Gagal menambahkan Paket Penerbitan');
				redirect(base_url('admin/konfig/paket'),'refresh',301);
			}			
		}else{
			$info['judul']="Tambah Paket Penerbitan";
			$this->load->view('admin/header',$info);
			$this->load->view('admin/konfig/v_paket_tambah');
			$this->load->view('admin/footer');
		}
	}

	function edit()
	{
		$this->form_validation->set_rules('paketID','ID Jenis Paket','required');
		$this->form_validation->set_rules('nama','Nama Paket Penerbitan','required');
		$this->form_validation->set_rules('harga','Harga Paket','required');
		$this->form_validation->set_rules('detail','Detail Paket','required');
		if($this->form_validation->run()==TRUE)
		{
			$pktID=$this->input->post('paketID',TRUE);
			$nama=$this->input->post('nama',TRUE);
			$harga=$this->input->post('harga',TRUE);
			$det=$this->input->post('detail',TRUE);
			
			if($this->konfig_model->paket_edit($pktID,$nama,$harga,$det)==TRUE)
			{
				set_header_message('success','Ubah Jenis Paket','Berhasil mengubah jenis paket penerbitan');
				redirect(base_url('admin/konfig/paket'),'refresh',301);
			}else{
				set_header_message('danger','Ubah Jenis Paket','Gagal mengubah jenis paket penerbitan');
				redirect(base_url('admin/konfig/paket'),'refresh',301);
			}			
		}else{
			$id=$this->input->get('id',TRUE);
			$info['judul']="Edit Jenis Paket";
			$this->load->view('admin/header',$info);
			$d['data']=$this->m_db->get_data('paket',array('id_paket'=>$id));
			$this->load->view('admin/konfig/v_paket_edit',$d);
			$this->load->view('admin/footer');
		}
	}

	function delete()
	{
		$id=$this->input->get('id',TRUE);
		if($this->konfig_model->paket_delete($id)==TRUE)
		{
			set_header_message('success','Hapus Paket Penerbitan','Berhasil menghapus paket penerbitan');
			redirect(base_url('admin/konfig/paket'),'refresh',301);
		}else{
			set_header_message('danger','Hapus Paket Penerbitan','Gagal menghapus paket penerbitan');
			redirect(base_url('admin/konfig/paket'),'refresh',301);
		}
	}

}