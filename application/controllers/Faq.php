<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Faq extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	}
	
	function index()
	{
		$meta['judul'] = baca_konfig('bikin-buku')." - FAQ";
		$data['faq'] = $this->m_db->get_data('faq');
		$this->load->view('html/header',$meta);
		$this->load->view('html/page/v_faq',$data);
		$this->load->view('html/footer');
	}
}