<!-- Footer -->
    <footer class="footer footer-black footer-big">
      <div class="container-fluid">

        <div class="content">
          <div class="row">
            <div class="col-md-4">
              <img class="responsive-img" src="<?=base_url();?>assets/img/logo-bottom.png"><br><br>
              <p style="color: #FFF;" class="text-right">Kami hadir memenuhi kebutuhan Anda dalam mengabadikan karya. Ingatan dapat hilang ditelan zaman, sedangkan tulisan abadi.</p>
              <br>
            </div>
            <div class="col-md-4">
              <h3 style="font-style: italic; color: #FFF;">“Orang boleh pandai setinggi langit, tapi selama ia tak menulis,
ia akan hilang di dalam masyarakat dan dari sejarah.”</h3>
              <h3 style="font-style: italic; color: #FFF;">― Pramoedya Ananta Toer, Rumah Kaca</h3>
            </div>
            <div class="col-md-4">
              <img class="responsive-img" src="<?=base_url();?>assets/img/swaterbit2.png"><br><br>
              <ul class="text-center">
                <li><a href=""><i class="fa fa-facebook fa-3x"></i></a></li>
                <li><a href=""><i class="fa fa-twitter fa-3x"></i></a></li>
                <li><a href=""><i class="fa fa-instagram fa-3x"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
        <hr />
        <ul class="pull-left hidden-xs">
          <li>
            <a href="#!">Blog</a>
          </li>
          <li>
            <a href="#!">Layanan</a>
          </li>
          <li>
            <a href="#!">Persyaratan</a>
          </li>
          <li>
            <a href="#!">FAQ</a>
          </li>
        </ul>

        <div class="copyright pull-right">
          Copyright &copy; <script>document.write(new Date().getFullYear())</script> Bikinbuku.co.id All Rights Reserved.<br>desain by devaraeko
        </div>
      </div>
    </footer>

  </div> <!-- End of Main -->

<!-- Core Files -->
  <script src="<?=base_url();?>assets/js/vendor/jquery-2.1.0.min.js"></script>
  <script src="<?=base_url();?>assets/js/vendor/bootstrap.min.js" type="text/javascript"></script>
  <script src="<?=base_url();?>assets/js/vendor/material.min.js"></script>
  
  <!-- Plugin for Select Form control, full documentation here: https://github.com/FezVrasta/dropdown.js -->
  <script src="<?=base_url();?>assets/js/vendor/jquery.dropdown.js" type="text/javascript"></script>
  <!-- Plugin for Tags, full documentation here: http://xoxco.com/projects/code/tagsinput/  -->
  <script src="<?=base_url();?>assets/js/vendor/jquery.tagsinput.js"></script>
  <!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
  <script src="<?=base_url();?>assets/js/vendor/jasny-bootstrap.min.js"></script>
  <!-- Control Center: activating the ripples, parallax effects, scripts from the example pages etc -->
  <script src="<?=base_url();?>assets/js/vendor/material-kit.js" type="text/javascript"></script>
  
  <!-- Side Navigation JS -->
  <script type="text/javascript" src="<?=base_url();?>assets/js/vendor/sidenav.js"></script>

  <!-- External Resources -->
  <script type="text/javascript" src="<?=base_url();?>assets/js/vendor/swiper.jquery.min.js"></script>
  <script type="text/javascript" src="<?=base_url();?>assets/js/vendor/chart.min.js"></script>
  <script type="text/javascript" src="<?=base_url();?>assets/js/vendor/headsup.js"></script>
  <script type="text/javascript" src="<?=base_url();?>assets/js/vendor/jquery.mixitup.min.js"></script>
  <script type="text/javascript" src="<?=base_url();?>assets/js/vendor/jquery.swipebox.min.js"></script>
  <script type="text/javascript" src="<?=base_url();?>assets/js/vendor/masonry.min.js"></script>

  <!-- Custom Javascript File -->
  <script src="<?=base_url();?>assets/js/main.js"></script>
</body>
</html>