<!DOCTYPE html>
<html class="no-js">
<head>
  <meta charset="utf-8">
  <title>Bikinbuku.co.id | Self Publishing</title>
  <meta name="description" content="">
  <meta name="HandheldFriendly" content="True">
  <meta name="MobileOptimized" content="320">
  <meta name="viewport" content="width=device-width, initial-scale=1, minimal-ui">

  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/touch/apple-touch-icon-144x144-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/touch/apple-touch-icon-114x114-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/touch/apple-touch-icon-72x72-precomposed.png">
  <link rel="apple-touch-icon-precomposed" href="img/touch/apple-touch-icon-57x57-precomposed.png">
  <link rel="shortcut icon" sizes="196x196" href="img/touch/touch-icon-196x196.png">
  <link rel="shortcut icon" href="img/touch/apple-touch-icon.png">

  <!-- Tile icon for Win8 (144x144 + tile color) -->
  <meta name="msapplication-TileImage" content="img/touch/apple-touch-icon-144x144-precomposed.png">
  <meta name="msapplication-TileColor" content="#222222">
  
  <!-- Reset browser default CSS -->
  <link href="<?=base_url();?>assets/css/reset.css" type="text/css" rel="stylesheet"/>

  <!-- SEO: If mobile URL is different from desktop URL, add a canonical link to the desktop page -->
  
  <link rel="canonical" href="http://bikinbuku.co.id/" > 

  <!-- Add to homescreen for Chrome on Android -->

  <meta name="mobile-web-app-capable" content="yes"> 

  <!-- For iOS web apps. Delete if not needed. https://github.com/h5bp/mobile-boilerplate/issues/94 -->

  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="black">
  <meta name="apple-mobile-web-app-title" content="">

  <!-- Fonts & Icons -->
  <link href="https://fonts.googleapis.com/css?family=Roboto%7CRoboto+Slab%7CMaterial+Icons" rel="stylesheet">
  <link rel="stylesheet" href="<?=base_url();?>assets/css/font-awesome.min.css">

  <!-- CSS Files -->
  <link href="<?=base_url();?>assets/css/bootstrap.min.css" rel="stylesheet" />
  <link href="<?=base_url();?>assets/css/material-kit.css" rel="stylesheet" />
  <link href="<?=base_url();?>assets/css/swiper.min.css" rel="stylesheet" />
  <link href="<?=base_url();?>assets/css/swipebox.min.css" rel="stylesheet" />
  <link href="<?=base_url();?>assets/css/main.css" rel="stylesheet" />
  
  <script src="<?=base_url();?>assets/js/vendor/modernizr-2.7.1.min.js"></script>

</head>
<body>

  <!-- Toolbar -->
  <div id="toolbar" class="white">
    <div class="open-left">
      <button type="button" class="button-collapse navbar-toggle collapsed" data-activates="slide-out" style="color: #3c69b1;">
        <span class="sr-only">Toggle navigation</span>
        <span class="material-icons">apps</span>
      </button>
    </div>
    <h1 class="branding" style="padding: 5px;">
      <center>
        <img src="<?=base_url();?>assets/img/materials.png" class="img-responsive" style="height: 45px;">
      </center>
    </h1>
    
    <div class="open-right" data-activates="slide-out-right">
      <i class="material-icons" style="color: #3c69b1;">person</i>
    </div>
  </div>
  <!-- End of Toolbar -->

  <!-- Left Sidebar -->
  <div id="slide-out" class="side-nav black">
    
    <div class="card side-nav-card">
      <div class="opacity-overlay"></div>
      <div class="content">
        <div class="footer">
          <div class="author">
                 <span class="info-title white-text">MENU</span>
          </div>
        </div>
      </div>
    </div>

    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel">
        <div class="panel-heading">
          <h4 class="panel-title"><a href="<?=base_url();?>"><i class="material-icons">home</i> Beranda</a></h4>
        </div>
      </div>
      <div class="panel">
        <div class="panel-heading">
          <h4 class="panel-title"><a href="<?=base_url();?>layanan"><i class="material-icons">add_to_queue</i> Layanan</a></h4>
        </div>
      </div>
      <div class="panel">
        <div class="panel-heading">
          <h4 class="panel-title"><a href="<?=base_url();?>persyaratan"><i class="material-icons">info</i> Persyaratan</a></h4>
        </div>
      </div>
      <div class="panel">
        <div class="panel-heading">
          <h4 class="panel-title"><a href="<?=base_url();?>faq"><i class="material-icons">live_help</i> Faq</a></h4>
        </div>
      </div>
      <div class="panel">
        <div class="panel-heading">
          <h4 class="panel-title"><a href="#">Contact</a></h4>
        </div>
      </div>
    </div>  
  </div>
  
  <!-- Right Sidebar -->
  <div id="slide-out-right" class="side-nav">
  <?php if(aktif()==1)
  {
  ?>
    <div class="card side-nav-card">
      <div class="opacity-overlay"></div>
      <div class="content">
        <div class="footer">
          <div class="author">
            <span class="info-title white-text">Hai, <?=user_info('nama');?></span>
          </div>
        </div>
      </div>
    </div>
    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel">
        <div class="panel-heading">
          <h4 class="panel-title"><a href="<?=base_url();?>profil"><i class="material-icons">account_circle</i> Profil</a></h4>
        </div>
      </div>
      <div class="panel">
        <div class="panel-heading">
          <h4 class="panel-title"><a href="<?=base_url();?>unggah"><i class="material-icons">file_upload</i> Unggah Naskah</a></h4>
        </div>
      </div>
      <div class="panel">
        <div class="panel-heading">
          <h4 class="panel-title"><a href="<?=base_url();?>status"><i class="material-icons">info</i> Status Pesanan</a></h4>
        </div>
      </div>
      <div class="panel">
        <div class="panel-heading">
          <h4 class="panel-title"><a href="<?=base_url();?>konfirmasi"><i class="material-icons">check_circle</i> Konfirmasi Pesanan</a></h4>
        </div>
      </div>
      <div class="divider"></div>
      <div class="panel">
        <div class="panel-heading">
          <h4 class="panel-title"><a href="<?=base_url();?>login/logout"><i class="material-icons">highlight_off</i> Keluar</a></h4>
        </div>
      </div> 
    </div>
  <?php
  } else{
    $ref=$this->uri->ruri_string();
  ?>
    <div class="card side-nav-card">
      <div class="opacity-overlay"></div>
      <div class="content">
        <div class="footer">
          <div class="author">
            <span class="info-title white-text">Halo, selamat datang!</span>
          </div>
        </div>
      </div>
    </div>
    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel">
        <div class="panel-heading">
          <h4 class="panel-title"><a href="<?=base_url();?>masuk"><i class="material-icons">input</i> Masuk</a></h4>
        </div>
      </div>
      <div class="panel">
        <div class="panel-heading">
          <h4 class="panel-title"><a href="<?=base_url();?>daftar"><i class="material-icons">add_circle_outline</i> Daftar</a></h4>
        </div>
      </div>      
    </div>
  <?php } ?>
  </div>

  <!-- The Main container have some space on top, so your toolbar will not cover the page contents -->
  <div id="main"> 
    <!-- Slider -->         
    <div class="swiper-container swiper-slider">
      <div class="swiper-wrapper">
        <div class="swiper-slide">
          <img src="<?=base_url();?>assets/img/bg0.jpg" alt="">
          <div class="opacity-overlay"></div>
          <div class="bottom text-center">
            <h4 class="white-text no-margin"><strong>Astro is There!</strong></h4>
            <p class="text-flow white-text no-margin">A mobile focused template</p>
          </div>
        </div>
        <div class="swiper-slide">
          <img src="<?=base_url();?>assets/img/bg3.jpg" alt="">
          <div class="opacity-overlay"></div>
          <div class="bottom text-left">
            <h4 class="white-text no-margin"><strong>Astro is There!</strong></h4>
            <p class="text-flow white-text no-margin">A mobile focused template</p>
          </div>
        </div>
        <div class="swiper-slide">
          <img src="<?=base_url();?>assets/img/bg2.jpg" alt="">
          <div class="opacity-overlay"></div>
          <div class="bottom text-right">
            <h4 class="white-text no-margin"><strong>Astro is There!</strong></h4>
            <p class="text-flow white-text no-margin">A mobile focused template</p>
          </div>
        </div>
      </div>
      <!-- Add Pagination -->
      <div class="swiper-pagination"></div>
    </div>
    <!-- End of Slider -->