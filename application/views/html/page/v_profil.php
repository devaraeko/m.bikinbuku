<style type="text/css">
  label {
    font-size: 16px;
  }
</style>
<div id="reading" class="content blog-post">
  <div class="main main-raised m-b-40">
    <div class="profile-content profile-page">
      <div class="container">
        <div class="row">
          <div class="col-xs-6 col-xs-offset-3">
            <div class="profile">
              <div class="avatar">
              <?php
              if (member_info('jk')=='laki-laki') { ?>
                <img src="<?=base_url()?>assets/img/user_men.png" alt="Circle Image" class="img-circle img-responsive img-raised">
              <?php } 
              else { ?>
                <img src="<?=base_url()?>assets/img/user_woman.png" alt="Circle Image" class="img-circle img-responsive img-raised">
              <?php } ?>
                
              </div>
              <div class="name">
                <h3 class="title"><?=member_info('nama');?></h3>                    
              </div>
            </div>
          </div>
          <div class="col-xs-2 follow">
            <a class="btn btn-fab btn-primary">
              <i class="material-icons">edit</i>
            </a>
          </div>
        </div>
      <div class="description text-center">
        <p>An artist of considerable range, Chet Faker — the name taken by Melbourne-raised, Brooklyn-based Nick urphy — writes, performs and records all of his own music, giving it a warm, intimate feel with a solid groove structure. </p>
      </div>

      <div class="row">
        <div class="col-md-6 col-md-offset-3">
          <div class="profile-tabs">
            <div class="nav-align-center">
              <ul class="nav nav-pills nav-pills-icons" role="tablist">
                <li class="active">
                  <a href="#work" role="tab" data-toggle="tab">
                    <i class="material-icons">book</i>
                      My books
                  </a>
                </li>
                <li>
                  <a href="#connections" role="tab" data-toggle="tab">
                    <i class="material-icons">face</i>
                      Profil
                  </a>
                </li>
                <li>
                  <a href="#media" role="tab" data-toggle="tab">
                    <i class="material-icons">account_balance</i>
                      Akun Bank
                  </a>
                </li>
              </ul>
            </div>
          </div>
            <!-- End Profile Tabs -->
        </div>
      </div>
      <div class="tab-content">
        <div class="tab-pane active work" id="work">
          <div class="row">
            <div class="col-md-7 col-md-offset-1">
              <h4 class="title">Buku Anda</h4>
                <div class="row collections">
                  <div class="col-md-6">
                    <div class="card card-background bgc4">
                      <a href="#!"></a>
                      <div class="content">
                        <a href="#!">
                          <h2 class="card-title">Judul buku</h2>
                        </a>
                        <label class="label label-primary">Kategori</label>                        
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="card card-background bgc4">
                      <a href="#!"></a>
                      <div class="content">
                        <a href="#!">
                          <h2 class="card-title">Judul buku</h2>
                        </a>
                        <label class="label label-primary">Kategori</label>                        
                      </div>
                    </div>
                  </div>
                </div>
            </div>
            <div class="col-md-2 col-md-offset-1 stats">
                <h4 class="title">Stats</h4>
                  <ul class="list-unstyled">
                            <li><b>60</b> Products</li>
                            <li><b>4</b> Collections</li>
                            <li><b>331</b> Influencers</li>
                            <li><b>1.2K</b> Likes</li>
                  </ul>
                        <hr />
                        <h4 class="title">Focus</h4>
                        <span class="label label-primary">Footwear</span>
                        <span class="label label-rose">Luxury</span>
            </div>
          </div>
        </div>
        <div class="tab-pane connections" id="connections">
          <div class="row">
            <div class="col-xs-12">
              <form>
                <div class="form-group">
                  <label class="col-xs-4 control-label">Nama lengkap</label>
                  <div class="col-xs-8">
                    <input type="text" name="" class="form-control" value="<?=member_info('nama');?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-xs-4 control-label">Nama pengguna</label>
                  <div class="col-xs-8">
                    <input type="text" name="" class="form-control" value="<?=member_info('username');?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-xs-4 control-label">Jenis kelamin</label>
                  <div class="col-xs-8">
                  <?php 
                  if (member_info('jk')=='laki-laki') { ?>
                     <div class="radio">
                      <label>
                        <input type="radio" name="optionsRadios" checked=""> Laki-laki
                      </label>
                    </div>
                    <div class="radio">
                      <label>
                        <input type="radio" name="optionsRadios"> Perempuan
                      </label>
                    </div>
                   <?php }
                   else { ?>
                    <div class="radio">
                      <label>
                        <input type="radio" name="optionsRadios"> Laki-laki
                      </label>
                    </div>
                    <div class="radio">
                      <label>
                        <input type="radio" name="optionsRadios" checked=""> Perempuan
                      </label>
                    </div>
                  <?php } ?>
                    
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-xs-4 control-label">Tanggal lahir</label>
                  <div class="col-xs-8">
                    <input type="date" name="" class="form-control" value="<?=member_info('tgl_lahir');?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-xs-4 control-label">No Telp</label>
                  <div class="col-xs-8">
                    <input type="text" name="" class="form-control" value="<?=member_info('telp');?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-xs-4 control-label">Facebook</label>
                  <div class="col-xs-8">
                    <input type="text" name="" class="form-control" value="<?=member_info('fb');?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-xs-4 control-label">Twitter</label>
                  <div class="col-xs-8">
                    <input type="text" name="" class="form-control" value="<?=member_info('twitter');?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-xs-4 control-label">Alamat</label>
                  <div class="col-xs-8">
                    <textarea class="form-control" placeholder="Alamat Anda" rows="3"><?=member_info('alamat');?></textarea>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
        <div class="tab-pane text-center gallery" id="media">
          <div class="row">
            <div class="col-xs-12">
              <form>
                <div class="form-group">
                  <label class="col-xs-4 control-label">Nama bank</label>
                  <div class="col-xs-8">
                    <input type="text" name="" class="form-control" value="<?=member_info('nama_bank');?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-xs-4 control-label">Kode bank</label>
                  <div class="col-xs-8">
                    <input type="text" name="" class="form-control" value="<?=member_info('kode_bank');?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-xs-4 control-label">No Rekening</label>
                  <div class="col-xs-8">
                    <input type="text" name="" class="form-control" value="<?=member_info('no_rek');?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-xs-4 control-label">Atas nama</label>
                  <div class="col-xs-8">
                    <input type="text" name="" class="form-control" value="<?=member_info('nama_rek');?>">
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  </div>
</div>