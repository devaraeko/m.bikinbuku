<div class="section white">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<form method="post" action="<?=base_url();?>unggah_naskah/unggah" enctype="multipart/form-data">
        <h1 class="text-center" style="font-weight: 600;">UNGGAH NASKAH</h1>
        <input type="hidden" name="memberid" value="<?=member_info('id');?>"/>
        <div class="col-xs-12">
        	<div class="form-group label-floating">
						<label class="control-label">Judul</label>
						<input type="text" name="judul" class="form-control" required="">
					</div>
        </div>
				<div class="col-xs-6">
					<div class="form-group label-floating">
						<label class="control-label">Penulis</label>
						<input type="text" name="penulis" class="form-control" required="">
					</div>
				</div>
				<div class="col-xs-6">
					<div class="form-group label-floating">
						<label class="control-label">Penerbit</label>
						<input type="text" name="penerbit" class="form-control" required="">
					</div>
				</div>
				<div class="col-xs-12">
					<div class="input-group required">
						<select name="ktgr" class="form-control" data-placeholder="Pilih">
							<option value="0">Pilih Kategori</option>
							<?php
									$kat=$this->m_db->get_data('kat_buku');
									if(!empty($kat))
									{
										foreach($kat as $kate)
										{
											echo '<option value="'.$kate->kat_id.'">'.$kate->nama.'</option>';
										}
									}
									?>
						</select>
					</div>
				</div>
				<div class="col-xs-6">
					<div class="input-group required">
						<select name="paket" class="form-control" id="hrg_pkt" onchange="hargapaket()">
							<option value="0">Pilih Paket</option>
							<?php
									$paket=$this->m_db->get_data('paket');
									if(!empty($paket))
									{
										foreach($paket as $pkt)
										{
											echo '<option value="'.$pkt->id_paket.'">'.$pkt->nama_paket.'</option>';
										}
									}
									?>
									<option value="10">Pilih Manual</option>
						</select>						
					</div>
				</div>
				<div class="col-xs-6">
					<div class="form-group label-floating">
						<label class="control-label"></label>
						<input type="text" name="harga_paket" id="harga_paket" class="form-control" readonly="">
					</div>
				</div>
				<div id="custom" style="display: none;">
					<div class="col-xs-12">
						<div class="checkbox">
							<label class="checkbox-inline"><input type="checkbox" name="tulis" value="1">penulisan</label>
							<label class="checkbox-inline"><input type="checkbox" name="sunting" value="1">penyuntingan</label>
							<label class="checkbox-inline"><input type="checkbox" name="alih" id="alih_b" value="1" onclick="alihbahasa()">alih bahasa</label>
						</div>					
					</div>
					<div class="col-xs-12" id="alih" style="display: none;">
						<div class="input-group required">
							<select class="form-control" name="sel_alihbahasa" id="sel_alihbahasa">
								<option value="0">Pilihan Alih Bahasa</option>
								<option value="1">Indonesia ke Inggris</option>
								<option value="2">Inggris ke Indonesia</option>
							</select>
						</div>
					</div>
					<div class="col-xs-12">
						<div class="checkbox">
							<label class="checkbox-inline"><input type="checkbox" name="rancang" value="1">rancang sampul</label>
							<label class="checkbox-inline"><input type="checkbox" name="tata" value="1">tata letak</label>
							<label class="checkbox-inline"><input type="checkbox" name="ilus" value="1">ilustrasi</label>
						</div>
					</div>
					<div class="col-xs-12">
						<div class="checkbox">
							<label class="checkbox-inline"><input type="checkbox" name="isbn" value="1">ISBN</label>
							<label class="checkbox-inline"><input type="checkbox" name="cetak" value="1" checked disabled>Cetak</label>
							<label class="checkbox-inline"><input type="checkbox" name="pasar" value="1">pemasaran</label>
						</div>
					</div>
				</div>
				
				<div class="col-xs-12">
					<div class="input-group required">
							<select id="pil_ker" name="kertas" class="form-control" onchange="ulanghitung()" data-placeholder="Pilih">
								<option value="0">Pilih Kertas</option>
								<?php
								$ker=$this->m_db->get_data('kertas');
								if(!empty($ker))
								{
									foreach($ker as $kts)
									{
										echo '<option value="'.$kts->id_kertas.'">'.$kts->nama_kertas.'</option>';
									}
								}
								?>
							</select>						
					</div>
				</div>
				<div class="col-xs-12">
					<a class="btn btn-danger" data-toggle="modal" data-target="#myModal">
					  Kalkulator
					</a>
				</div>
				<div class="col-xs-6">
					<div class="form-group label-floating">
						<label class="control-label">Halaman hitam putih</label>
						<input type="text" name="jum_hitam" id="jum_hitam"  class="form-control" value="0" readonly="">
					</div>
				</div>
				<div class="col-xs-6">
					<div class="form-group label-floating">
						<label class="control-label">Halaman warna</label>
						<input type="text" name="jum_warna" id="jum_warna" class="form-control" value="0" readonly="">
					</div>
				</div>
				<div class="col-xs-6">
					<div class="form-group label-floating">
						<label class="control-label">Jumlah cetakan</label>
						<input type="text" name="jum_ctk" id="jum_ctk" class="form-control" value="0" readonly="">
					</div>
				</div>
				<div class="col-xs-6">
					<div class="form-group label-floating">
						<label class="control-label">Harga per exemplar</label>
						<input type="text" name="harga_cetak" id="harga_cetak" class="form-control" value="0" readonly="">
					</div>
				</div>
				<div class="col-xs-6">
					<div class="form-group label-floating">
						<label class="control-label">Harga total</label>
						<input type="text" name="harga_total" id="harga_total" class="form-control" value="0" readonly="">
					</div>
				</div>
				<div class="col-xs-6">
					<div class="form-group label-floating">
						<label class="control-label">Harga jual konsumen</label>
						<input type="text" name="harga_konsumen" id="harga_konsumen" class="form-control" value="0" readonly="">
					</div>
				</div>
				<div class="col-xs-12">
					<textarea name="catatan" class="form-control" placeholder="Catatan Anda" rows="3"></textarea>
				</div>
				<div class="col-xs-12">
					<div class="form-group">
						<a class="btn btn-danger"><input type="file" name="naskah" class="form-control">Pilih</a>
						<label class="control-label">File naskah</label>
					</div>
				</div>
				<div class="col-xs-12">
					<div class="form-group">
						<a class="btn btn-danger"><input type="file" name="cover" class="form-control">Pilih</a>
						<label class="control-label">File Cover <i>(CDR Recomended)</i></label>
					</div>
				</div>
				<div class="col-xs-12">
					<div class="form-group">
						<button type="submit" id="simpan" class="btn btn-primary btn-flat">Simpan</button>
					</div>
				</div>
        </form>

					<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					  <div class="modal-dialog">
					    <div class="modal-content">
					      <div class="modal-header">
					        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					        <h4 class="modal-title" id="myModalLabel">Kalkulator Produksi</h4>
					      </div>
					      <div class="modal-body">
					        <form action="javascript:hitung()">
					        	<div class="col-xs-12">
					        		<div class="form-group required">
													<select name="kertas" class="form-control" id="kertas_id" onchange="tampilKertas()" placeholder="Pilih Kertas" required="">
														<option value="0">Pilih Kertas</option>
														<?php
														$ker=$this->m_db->get_data('kertas');
														if(!empty($ker))
														{
															foreach($ker as $kts)
															{
																echo '<option value="'.$kts->id_kertas.'">'.$kts->nama_kertas.'</option>';
															}
														}
														?>
													</select>
											</div>
					        	</div>
										<div class="col-xs-6">
											<div class="form-group">
												<label class="control-label" for="">Halaman Hitam Putih</label>
												<input type="number" name="jml_hp" id="jml_hp" value="100" class="form-control" min="100" required="">
											</div>
										</div>
										<div class="col-xs-6">
											<div class="form-group">
												<label class="control-label" for="">Harga</label>
												<input type="text" name="isi_hp" id="ket1" class="form-control" readonly="">
											</div>
										</div>
										<div class="col-xs-6">
											<div class="form-group">
												<label class="control-label" for="">Halaman Berwarna</label>
												<input type="text" name="jml_warna" id="jml_warna" value="0" class="form-control" required="">
											</div>
										</div>
										<div class="col-xs-6">
											<div class="form-group">
												<label class="control-label" for="">Harga</label>
												<input type="text" name="isi_bw" id="ket2" class="form-control" readonly="">
											</div>
										</div>
										<div class="col-xs-12">
											<div class="form-group required">
												<label class="control-label">Jumlah Cetakan(exemplar)</label>
												<input type="text" name="jml_cetak" id="jml_cetak" class="form-control" required="">
											</div>
										</div>
										
										<div class="col-xs-6">
											<div class="form-group required">
												<label class="control-label">Cover</label>
												<input type="text" name="cover" id="cover" class="form-control" disabled="" value="3000">
											</div>
										</div>
										<div class="col-xs-6">
											<div class="form-group required">
												<label class="control-label">Laminasi</label>
												<input type="text" name="laminasi" id="laminasi" class="form-control" disabled="" value="2500">
											</div>
										</div>
										<div class="col-xs-6">
											<div class="form-group required">
												<label class="control-label">Perfect Binding</label>
												<input type="text" name="binding" id="binding" class="form-control" disabled="" value="3000">
											</div>
										</div>
										<div class="col-xs-6">
											<div class="form-group required">
												<label class="control-label">Wrapping</label>
												<input type="text" name="wrap" id="wrap" class="form-control" disabled="" value="500">
											</div>
										</div>
										<div class="col-xs-6">
											<button type="submit" id="htg_btn" class="btn btn-info" disabled="">Hitung</button>
										</div>
										<div class="col-xs-6">
											<label class="control-label"><p id="hitung_alert" style="display: none; font-size: 12px; font-style: italic; color: red;">Hitung</p></label>
										</div>
					        </form>
					        <form action="javascript:set()">
					        	<div class="col-xs-12">
											<div class="form-group required">
												<label class="control-label">Harga per Exemplar</label>
												<input type="text" name="harga_produksi" id="harga_produksi" class="form-control" readonly="">
											</div>
										</div>
										<div class="col-xs-12">
											<div class="form-group required">
												<label class="control-label">Harga Total Produksi</label>
												<input type="text" name="total_produksi" id="total_produksi" class="form-control" readonly="">
											</div>
										</div>
										<div class="col-xs-12">
											<div class="form-group required">
												<label class="control-label">Harga Jual Konsumen</label>
												<input type="text" name="jual_konsumen" id="jual_konsumen" class="form-control">
											</div>
										</div>
										<div class="col-xs-12">
											<button type="submit" id="set" class="btn btn-info" disabled="">Set</button>
										</div>
										<div class="col-xs-12">
											<p id="alert" style="color: red; font-size: 12px; font-style: italic; display: none;">Minimum Rp 10.000 diatas harga per exemplar</p>
										</div>
					        </form>
					      </div>
					      <div class="modal-footer">
					      	<div class="col-xs-6 col-xs-offset-6">
					      		<button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
					      	</div>
					      </div>
					    </div>
					  </div>
					</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	function tampilKertas()
	 {
		 kdprop = document.getElementById("kertas_id").value;
		 $.ajax({
			 url:"<?php echo base_url();?>unggah_naskah/tampilkertas/"+kdprop+"",
			 success: function(response){
			 document.getElementById('ket1').value=parseInt(response.hitamputih);
			 document.getElementById('ket2').value=parseInt(response.warna);
			 $('#hitung_alert').show('slow');
			 document.getElementById('htg_btn').disabled = false;
			 document.getElementById('set').disabled = true;
			 },
			 dataType:"json"
		 });
	 }

	 function hargapaket()
	 {
	 		var id_paket=document.getElementById('hrg_pkt').value;
	 		if (id_paket==10) {
	 			document.getElementById('harga_paket').value=parseInt(0);
	 			$('#custom').show('slow');
	 		}
	 		else {
	 			$.ajax({
					url:"<?php echo base_url();?>unggah_naskah/tampilpaket/"+id_paket+"",
					success: function(response){
					document.getElementById('harga_paket').value=parseInt(response.harga);
					$('#custom').hide('slow');
					},
					dataType:"json"
				 });
	 		}
	 		
		 return false;
	 }

	 function alihbahasa() {
	 	if (document.getElementById('alih_b').checked) 
		{
		    $('#alih').show('slow');
		} else {
			$('#alih').hide('slow');
			document.getElementById('sel_alihbahasa').value=parseInt(0);
		  }
	 	
	 }

	 function kalkulator()
	 {
	 	$("#kalkulator").show('slow');
	 	$('#kertas_alert').html('');

	 }

	 function hitung()
	 {
	 		var htm= $("input#jml_hp").val();
	 		var wrn=$("input#jml_warna").val();
	 		var hrg_htm=$("input#ket1").val();
	 		var hrg_wrn=$("input#ket2").val();
	 		var jml_ctk=$("input#jml_cetak").val();
	 		var cov=$("input#cover").val();
	 		var lam=$("input#laminasi").val();
	 		var bin=$("input#binding").val();
	 		var wrap=$("input#wrap").val();
	 		$.ajax({
	 			type: "POST",
				url: "<?php echo base_url(); ?>/unggah_naskah/hitung",
				data: {
					hitam: htm,
					warna: wrn,
					hrg_hitam: hrg_htm,
					hrg_warna: hrg_wrn,
					jml_cetak: jml_ctk,
					cover: cov,
					lami: lam,
					bind: bin,
					wrapping: wrap
				},
				success: function(hasil)
				{
					if (hasil) {
						$('#hitung_alert').hide('slow');
						document.getElementById('harga_produksi').value=parseInt(hasil.satu_produk);
			 			document.getElementById('total_produksi').value=parseInt(hasil.total_produk);
			 			document.getElementById('set').disabled = false;
					}
					else {
						$("#harga_produksi").val(0);
						$("#total_produksi").val(0);
					}
				},
				dataType: "json"
	 		});
	 }

	 function set()
	 {
	 		var pil_kts=$("#kertas_id").val();
	 		var htm= $("input#jml_hp").val();
	 		var wrn=$("input#jml_warna").val();
	 		var jml_ctk=$("input#jml_cetak").val();
	 		var per_buku=parseInt(document.getElementById('harga_produksi').value);
	 		var total=parseInt(document.getElementById('total_produksi').value);
			var konsumen=parseInt(document.getElementById('jual_konsumen').value);
			var selisih=konsumen-per_buku;
			if(selisih>=10000)
			{
				$('#pil_ker').val(pil_kts);
				$('input#jum_hitam').val(htm);
				$('input#jum_warna').val(wrn);
				$('input#jum_ctk').val(jml_ctk);
				$('input#harga_cetak').val(per_buku);
				$('input#harga_total').val(total);
				$('input#harga_konsumen').val(konsumen);
				$("#alert").hide('slow');
				$("#kalkulator").hide('slow');
				$('#kertas_alert').html('Jangan diganti');
				document.getElementById('simpan').disabled = false;
			}
			else
			{
				document.getElementById('jum_hitam').value=parseInt(0);
				document.getElementById('jum_warna').value=parseInt(0);
				document.getElementById('jum_ctk').value=parseInt(0);
				document.getElementById('harga_cetak').value=parseInt(0);
				document.getElementById('harga_total').value=parseInt(0);
				document.getElementById('harga_konsumen').value = 0;
				document.getElementById('simpan').disabled = true;
				$("#alert").show('slow');
			}
	 }

	function ulanghitung()
	{
		$('#kertas_alert').html('Ulangi kalkulasi');
		document.getElementById('jum_hitam').value=parseInt(0);
		document.getElementById('jum_warna').value=parseInt(0);
		document.getElementById('jum_ctk').value=parseInt(0);
		document.getElementById('harga_cetak').value=parseInt(0);
		document.getElementById('harga_total').value=parseInt(0);
		document.getElementById('harga_konsumen').value = 0;
		document.getElementById('simpan').disabled = true;
	}
</script>