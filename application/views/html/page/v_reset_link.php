<section id="daftar">
  <div class="row">
    <div class="col-md-10 col-md-offset-1">
      <div class="title-area">
          <h2>Reset Kata Sandi</h2>
      </div>
    </div>    
  </div>
	<div class="row">
		<div class="col-md-8 col-md-offset-2">			
		  <?php
        echo form_open(base_url('member/apply_reset'),array('class'=>'form-horizontal'));
        ?>
        <div class="form-group">
        	<label class="control-label col-md-3"></label>
        	<div class="col-md-7">
						<p>Silakan masukkan kata sandi baru Anda</p>
					</div>
        </div>
        <input type="hidden" name="IDmember" value="<?=$key;?>">
        <div class="form-group has-feedback has-show">
        	<label class="control-label col-md-3">Kata sandi baru Anda</label>
        	<div class="col-md-7">
						<input type="password" name="pass" class="form-control passwordfield" autocomplete="off" placeholder="Masukkan kata sandi baru Anda" required="" autofocus="" />
            <span class="form-control-clear glyphicon glyphicon-eye-open form-control-feedback hidden"></span>
					</div>
        </div>
        <div class="form-group">
        	<label class="control-label col-md-3"></label>
        	<div class="col-md-7">
						<button type="submit" class="btn btn-primary">Reset Password</button>
					</div>
        </div>
        <?php
        echo form_close();      
        ?>
		</div>
	</div>
</section>