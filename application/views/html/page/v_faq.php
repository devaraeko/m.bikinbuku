<div class="section">
  <div class="container">
    <div class="card">
      <div class="content">
        <h2 class="title">Frequently Asked Questions</h2>
        <div id="acordeon">
          <div class="panel-group" id="product-accordion" role="tablist">
          <?php 
          foreach ($faq as $r) { ?>
            <div class="panel panel-border panel-default">
              <div class="panel-heading" role="tab" id="product-headingOne">
                <a role="button" data-toggle="collapse" data-parent="#product-accordion" href="#product-collapse<?=$r->id_faq?>" aria-expanded="true" aria-controls="product-collapse<?=$r->id_faq?>">
                  <h4 class="panel-title">
                    <i class="material-icons">keyboard_arrow_down</i>
                    <?=$r->judul?>                  
                  </h4>
                </a>
              </div>
              <div id="product-collapse<?=$r->id_faq?>" class="panel-collapse collapse">
                <div class="panel-body">
                  <?=$r->isi?>
                </div>
              </div>
            </div>
          <?php } ?> 
          </div>
        </div><!--  end acordeon -->
      </div>
    </div>
  </div>
</div>