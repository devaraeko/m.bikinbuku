<div class="section white">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="table-responsive">
					<table class="table">
				    <thead>
				    	<tr style="color: #3c69b1;">
								<th class="text-center">Kode Order</th>
					      <th class="text-center">Tanggal Masuk Order</th>
					      <th class="text-center">Judul</th>
					      <th class="text-center">Total Biaya</th>
					      <th class="text-center">Status</th>
					      <th class="text-center"></th>
							</tr>
				    </thead>
					    <tbody>
					        <?php
							if (!empty($data)) {
								foreach ($data as $row) {
									$judul = field_value('project_detail','id_project',$row->id_project,'judul');
									$id = $row->id_project;
									foreach ($det as $d) {
										
									}
									?>
									<tr style="text-align: center;">
										<td><?=$row->kode_order?></td>
										<td><?=$row->tgl_order?></td>
										<td><?=$judul;?></td>
										<td>Rp. <?php echo number_format($d->total_semua,2,",",".") ?></td>
										<td><?=$row->status?></td>
										<td>
											<a href="javascript:tampildetail('<?=$id;?>')" class="show_detail" id="<?=$id;?>">Detail</a>
										</td>
									</tr>
								<?php	
								}
							}
							else {
								echo "Belum ada";
							}
						?>
					    </tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>