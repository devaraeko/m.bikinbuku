
    <div class="section white">

    <div class="container">
      <div class="row">
        <div class="col-sm-3">
          <div class="info text-center">
            <div class="icon">
              <i class="material-icons">thumb_up</i>
            </div>
            <h4 class="info-title">Cepat dan Berkualitas</h4>
            <p>Nikmati layanan terbaik dari kami. Yang pasti cepat serta berkualitas.</p>
          </div>
        </div>

        <div class="col-sm-3">
          <div class="info text-center">
            <div class="icon">
              <i class="material-icons">dashboard</i>
            </div>
            <h4 class="info-title">Tersedia Berbagai Paket</h4>
            <p>Pilih salah satu saran dan paket yang kami sesuaikan untuk kebutuhan Anda.</p>
          </div>
        </div>

        <div class="col-sm-3">
          <div class="info text-center">
            <div class="icon">
              <i class="material-icons">format_paint</i>
            </div>
            <h4 class="info-title">Desain Berkualitas</h4>
            <p>Anda tidak perlu takut jika tidak mempunyai kemampuan desain untuk cover buku Anda. Kami siap merancang desain rupa untuk buku Anda dengan hasil terbaik.</p>
          </div>
        </div>
        <div class="col-sm-3">
          <div class="info text-center">
            <div class="icon">
              <i class="material-icons">print</i>
            </div>
            <h4 class="info-title">Print On Demand</h4>
            <p>Semua pekerjaan seperti editing, layout, cetak dan distribusi dilakukan oleh kami sendiri dengan kualitas terbaik.</p>
          </div>
        </div>
      </div>

    </div>
  </div>
  <!-- End of Features -->
  <div class="container white">
    <div class="card">
      <div class="content content-info">
        <h4 class="card-title">
          <a href="#!">"Terbitkan Naskahmu Sekarang"</a>
        </h4>
        <p class="card-description">
          Nikmati layanan terbaik dari kami!
        </p>
        <div class="footer text-center">
          <a href="<?=base_url();?>unggah" class="btn" style="background: #3c69b1;">Unggah Naskah Sekarang</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Featured -->
  <div class="container p-t-40 text-center light-grey">
    <h2 class="info-title">Ikuti Mereka!</h2>
    <p>Menjadi penulis yang tidak takut untuk menerbitkan karyanya!</p>
  </div>

   <!-- Testimonials Slider -->
        <!-- Slider -->         
        <div class="swiper-container testimonials white">
          <div class="swiper-wrapper">
            <div class="swiper-slide center">
              <p class="testimonial">"It wasn't a dream. His room, a proper human room although a little too small, lay peacefully between its four familiar walls. A collection of textile samples lay spread out on the table - Samsa was a travelling salesman - and above it there hung a picture that he had recently cut out of an illustrated magazine and housed in a nice, gilded frame."</p>
              <br>
              <h6 class="m-0">Jane Lobster</h6>
              <p class="small">Art Director, BeeQuee</p>
            </div>
            <div class="swiper-slide center">
              <p class="testimonial">"It wasn't a dream. His room, a proper human room although a little too small, lay peacefully between its four familiar walls. A collection of textile samples lay spread out on the table - Samsa was a travelling salesman - and above it there hung a picture that he had recently cut out of an illustrated magazine and housed in a nice, gilded frame."</p>
              <br>
              <h6 class="m-0">Rick Bell</h6>
              <p class="small">Photograper, Larey-q</p>
            </div>
            <div class="swiper-slide center">
              <p class="testimonial">"It wasn't a dream. His room, a proper human room although a little too small, lay peacefully between its four familiar walls. A collection of textile samples lay spread out on the table - Samsa was a travelling salesman - and above it there hung a picture that he had recently cut out of an illustrated magazine and housed in a nice, gilded frame."</p>
              <br>
              <h6 class="m-0">Dora Moore</h6>
              <p class="small">Illustrator, Arque</p>
            </div>
          </div>
          <!-- Add Pagination -->
          <div class="swiper-pagination"></div>
        </div>
        <!-- End of Slider -->
  <!-- End of Slider -->