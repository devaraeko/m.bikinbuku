<!DOCTYPE html>
<html class="no-js">
<head>
  <meta charset="utf-8">
  <title>Astro - Mobile Template</title>
  <meta name="description" content="">
  <meta name="HandheldFriendly" content="True">
  <meta name="MobileOptimized" content="320">
  <meta name="viewport" content="width=device-width, initial-scale=1, minimal-ui">

  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/touch/apple-touch-icon-144x144-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/touch/apple-touch-icon-114x114-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/touch/apple-touch-icon-72x72-precomposed.png">
  <link rel="apple-touch-icon-precomposed" href="img/touch/apple-touch-icon-57x57-precomposed.png">
  <link rel="shortcut icon" sizes="196x196" href="img/touch/touch-icon-196x196.png">
  <link rel="shortcut icon" href="img/touch/apple-touch-icon.png">

  <!-- Tile icon for Win8 (144x144 + tile color) -->
  <meta name="msapplication-TileImage" content="img/touch/apple-touch-icon-144x144-precomposed.png">
  <meta name="msapplication-TileColor" content="#222222">
  
  <!-- Reset browser default CSS -->
  <link href="<?=base_url();?>assets/css/reset.css" type="text/css" rel="stylesheet"/>

  <!-- SEO: If mobile URL is different from desktop URL, add a canonical link to the desktop page -->
  <!-- 
    <link rel="canonical" href="http://www.example.com/" > 
  -->

  <!-- Add to homescreen for Chrome on Android -->
  <!-- 
    <meta name="mobile-web-app-capable" content="yes"> 
  -->

  <!-- For iOS web apps. Delete if not needed. https://github.com/h5bp/mobile-boilerplate/issues/94 -->
  <!--
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-title" content="">
  -->

  <!-- Fonts & Icons -->
  <link href="https://fonts.googleapis.com/css?family=Roboto%7CRoboto+Slab%7CMaterial+Icons" rel="stylesheet">
  <link rel="stylesheet" href="<?=base_url();?>assets/css/font-awesome.min.css">

  <!-- CSS Files -->
  <link href="<?=base_url();?>assets/css/bootstrap.min.css" rel="stylesheet" />
  <link href="<?=base_url();?>assets/css/material-kit.css" rel="stylesheet" />
  <link href="<?=base_url();?>assets/css/swiper.min.css" rel="stylesheet" />
  <link href="<?=base_url();?>assets/css/swipebox.min.css" rel="stylesheet" />
  <link href="<?=base_url();?>assets/css/main.css" rel="stylesheet" />
  
  <script src="<?=base_url();?>assets/js/vendor/modernizr-2.7.1.min.js"></script>

</head>
<body>

  <!-- Toolbar -->
  <div id="toolbar" class="white">
    <div class="open-left">
      <button type="button" class="button-collapse navbar-toggle collapsed" data-activates="slide-out" style="color: #3c69b1;">
        <span class="sr-only">Toggle navigation</span>
        <span class="material-icons">apps</span>
      </button>
    </div>
    <h1 class="branding" style="padding: 5px; margin-right: 20px;">
      <center>
        <img src="<?=base_url();?>assets/img/materials.png" class="img-responsive" style="height: 45px;">
      </center>
    </h1>
  </div>
  <!-- End of Toolbar -->

  <!-- Left Sidebar -->
  <div id="slide-out" class="side-nav">
    
    <div class="card side-nav-card">
      <div class="opacity-overlay"></div>
      <div class="content">
        <div class="footer">
          <div class="author">
                 <span class="info-title white-text">MENU</span>
          </div>
        </div>
      </div>
    </div>

    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel">
        <div class="panel-heading">
          <h4 class="panel-title"><a href="<?=base_url();?>"><i class="material-icons">home</i> Beranda</a></h4>
        </div>
      </div>
      <div class="panel">
        <div class="panel-heading">
          <h4 class="panel-title"><a href="<?=base_url();?>layanan"><i class="material-icons">add_to_queue</i> Layanan</a></h4>
        </div>
      </div>
      <div class="panel">
        <div class="panel-heading">
          <h4 class="panel-title"><a href="<?=base_url();?>persyaratan"><i class="material-icons">info</i> Persyaratan</a></h4>
        </div>
      </div>
      <div class="panel">
        <div class="panel-heading">
          <h4 class="panel-title"><a href="<?=base_url();?>faq"><i class="material-icons">live_help</i> Faq</a></h4>
        </div>
      </div>
      <div class="panel">
        <div class="panel-heading">
          <h4 class="panel-title"><a href="#">Contact</a></h4>
        </div>
      </div>
    </div>  
  </div>
 

  <!-- The Main container have some space on top, so your toolbar will not cover the page contents -->
  <div id="main">
    <div class="page-header">
      <div class="container">
        <div class="card">
          <div class="content">
            <h2 class="card-title">
              RESET KATA SANDI
            </h2>
            <span>Belum punya akun bikinbuku? <a href="<?=base_url();?>daftar">Daftar</a></span>
            <form class="form">
              <div class="input-group">
                <span class="input-group-addon">
                  <i class="material-icons">mail_outline</i>
                </span>
                <input type="email" name="email" class="form-control" placeholder="Alamat surel..." required="">
              </div>
                <button type="submit" class="btn btn-primary">Kirim Link</button>
             
            </form>
          </div>
        </div>
        <footer class="footer">
            <div class="copyright black-text">
              &copy; <script>document.write(new Date().getFullYear())</script> Bikinbuku.co.id All right reserved
            </div>
          </footer>
      </div>
    </div>
  </div> <!-- End of Main -->

<!-- Core Files -->
  <script src="<?=base_url();?>assets/js/vendor/jquery-2.1.0.min.js"></script>
  <script src="<?=base_url();?>assets/js/vendor/bootstrap.min.js" type="text/javascript"></script>
  <script src="<?=base_url();?>assets/js/vendor/material.min.js"></script>
  
  <!-- Plugin for Select Form control, full documentation here: https://github.com/FezVrasta/dropdown.js -->
  <script src="<?=base_url();?>assets/js/vendor/jquery.dropdown.js" type="text/javascript"></script>
  <!-- Plugin for Tags, full documentation here: http://xoxco.com/projects/code/tagsinput/  -->
  <script src="<?=base_url();?>assets/js/vendor/jquery.tagsinput.js"></script>
  <!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
  <script src="<?=base_url();?>assets/js/vendor/jasny-bootstrap.min.js"></script>
  <!-- Control Center: activating the ripples, parallax effects, scripts from the example pages etc -->
  <script src="<?=base_url();?>assets/js/vendor/material-kit.js" type="text/javascript"></script>
  
  <!-- Side Navigation JS -->
  <script type="text/javascript" src="<?=base_url();?>assets/js/vendor/sidenav.js"></script>

  <!-- External Resources -->
  <script type="text/javascript" src="<?=base_url();?>assets/js/vendor/swiper.jquery.min.js"></script>
  <script type="text/javascript" src="<?=base_url();?>assets/js/vendor/chart.min.js"></script>
  <script type="text/javascript" src="<?=base_url();?>assets/js/vendor/headsup.js"></script>
  <script type="text/javascript" src="<?=base_url();?>assets/js/vendor/jquery.mixitup.min.js"></script>
  <script type="text/javascript" src="<?=base_url();?>assets/js/vendor/jquery.swipebox.min.js"></script>
  <script type="text/javascript" src="<?=base_url();?>assets/js/vendor/masonry.min.js"></script>

  <!-- Custom Javascript File -->
  <script src="<?=base_url();?>assets/js/main.js"></script>
</body>
</html>