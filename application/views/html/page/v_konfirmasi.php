<div class="section white">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<h1 class="text-center" style="font-weight: 700; color: #3c69b1;">KONFIRMASI PEMBAYARAN</h1>
				<form method="post" action="<?=base_url();?>unggah_naskah/unggah" enctype="multipart/form-data">
					<div class="form-group">
            <label class="col-xs-4 control-label">Kode Order</label>
              <div class="col-xs-8">
              	<select name="IDproject" id="kode" class="form-control" onchange="tagihanShow()">
              		<option value="0">--Pilih--</option>
              		<?php
									$pro=$this->m_db->get_data('project',array('status'=>'menunggu'));
									if(!empty($pro))
									{
										foreach($pro as $p)
										{
											echo '<option value="'.$p->id_project.'">'.$p->kode_order.'</option>';
										}
									}
									?>
              	</select>
              </div>
          </div>
          <div class="form-group" id="alert" style="display: none;">
          	<div class="col-xs-12">
          		<p style="font-size: 12px; font-style: italic; color: red;">Pilih kode order</p>
          	</div>
          </div>
          <div class="form-group">
          	<label class="col-xs-4 control-label">Tagihan</label>
          	<div class="col-xs-8">
          		<input type="text" name="tagihan" id="tagihan" class="form-control" readonly="">
          	</div>
          </div>
          <div class="form-group">
          	<div class="col-xs-12">
          		<a class="btn btn-danger"><input type="file" name="bukti" class="form-control" required="">Pilih</a>
          		<label class="control-label">File bukti pembayaran</label>
          	</div>
          </div>
          <div class="form-group">
          	<div class="col-xs-12">
          		<button type="submit" class="btn btn-info" id="kirim" disabled="">Upload bukti pembayaran</button>
          	</div>
          </div>
				</form>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	function tagihanShow()
	 {
	 		var kd=document.getElementById('kode').value;
	 		if (kd==0) {
	 			document.getElementById('tagihan').value=parseInt(0);
	 			$('#alert').show('slow');
	 			document.getElementById('kirim').disabled = true;
	 		}
	 		else {
	 			$.ajax({
					url:"<?php echo base_url();?>unggah_naskah/tampil_tagihan/"+kd+"",
					success: function(response){
					document.getElementById('tagihan').value=parseInt(response.harga);
					$('#alert').hide('slow');
					document.getElementById('kirim').disabled = false;
					},
					dataType:"json"
				 });
	 		}
	 		
		 return false;
	 }
</script>