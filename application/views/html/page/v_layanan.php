<style type="text/css">
  h4 {
    color: #3c69b1;
    font-weight: 700;
  }
</style>
<div class="content">
  <div class="container no-padding">
		<div class="col-xs-12 col-sm-6 col-md-4"> <!-- Left column on large screens -->
			<div class="card card-blog">
        <div class="card-image">
          <a>
            <img class="img" src="<?=base_url()?>assets/img/redaksi.png" alt="" />
          </a>
        </div>
        <div class="content">
          <div class="text-center">
						<h4 class="wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.5s">PENULISAN</h4>
						<p class="wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.5s">(gost writing)</p>
						<h4 class="wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.5s">PENYUNTINGAN</h4>
						<p class="wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.5s">(editing)</p>
						<h4 class="wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.5s">ALIH BAHASA</h4>
						<p class="wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.5s">(translating)</p>
					</div>
        </div>
      </div>
      
      
		</div>
		<div class="col-xs-12 col-sm-6 col-md-4">
			<div class="card card-blog">
        <div class="card-image">
          <a>
            <img class="img" src="<?=base_url()?>assets/img/rancang.png" alt="" />
          </a>
        </div>
        <div class="content">
          <div class="text-center">
						<h4 class="wow fadeInUp" data-wow-duration="0.75s" data-wow-delay="0.75s">RANCANG SAMPUL</h4>
						<p class="wow fadeInUp" data-wow-duration="0.75s" data-wow-delay="0.75s">(cover design)</p>
						<h4 class="wow fadeInUp" data-wow-duration="0.75s" data-wow-delay="0.75s">TATA LETAK</h4>
						<p class="wow fadeInUp" data-wow-duration="0.75s" data-wow-delay="0.75s">(layout)</p>
						<h4 class="wow fadeInUp" data-wow-duration="0.75s" data-wow-delay="0.75s">ILUSTRASI</h4>
						<p class="wow fadeInUp" data-wow-duration="0.75s" data-wow-delay="0.75s">(illustration)</p>
					</div>
        </div>
      </div>
		</div>
		<div class="col-xs-12 col-sm-6 col-md-4">
			<div class="card card-blog">
        <div class="card-image">
          <a>
            <img class="img" src="<?=base_url()?>assets/img/terbit.png" alt="" />
          </a>
        </div>
        <div class="content">
          <div class="text-center">
						<h4 class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="1s">ISBN</h4>
						<p class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="1s">(isbn)</p>
						<h4 class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="1s">CETAK</h4>
						<p class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="1s">(printing)</p>
						<h4 class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="1s">PEMASARAN</h4>
						<p class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="1s">(marketing)</p>
					</div>
        </div>
      </div>
		</div>
	</div>
	<div class="container">
		<div class="col-md-6">
			<div style="padding: 10px;">
				<a href="<?=base_url();?>unggah">
	        <img class="img" src="<?=base_url()?>assets/img/unggah.png" alt="" />
	      </a>
			</div>
		</div>
		<div class="col-md-6">
			<div style="padding: 10px;">
				<a href="http://template.bikinbuku.co.id" onclick="window.open(this.href, 'UnduhTemplate', 'resizable=no,status=no,location=no,toolbar=no,menubar=no,fullscreen=no,scrollbars=yes,dependent=no,width=600,height=600'); return false;">
	        <img class="img" src="<?=base_url()?>assets/img/unduh.png" alt="" />
	      </a>
			</div>
		</div>
	</div>
	<div class="container">
    <div class="text-center">
    	<h1 class="title">BINGUNG?</h1>
    	<h4>Pilih salah satu saran dan paket yang kami sesuaikan untuk kebutuhan Anda.</h4>
    </div>
  </div>

  <div class="container">
  	<div class="col-md-4">
  		<div class="card card-blog">
        <div class="card-image">
          <a>
            <img class="img" src="<?=base_url()?>assets/img/guru.png" alt="" />
          </a>
        </div>
        <div class="content">
          <?php $hrg_guru = field_value('paket','id_paket','1','harga');?>
          <a class="btn btn-primary">Rp. <?=number_format($hrg_guru,2,",",".") ?></a>
          <div class="panel-group">
            <div class="panel panel-border panel-default">
              <div class="panel-heading" role="tab" id="headingOne">
                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                  <h4 class="panel-title">Detail<i class="material-icons">keyboard_arrow_down</i></h4>
                </a>
              </div>
              <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body">
                  <p>
                    Lorem Ipsum adalah contoh teks atau dummy dalam industri percetakan dan penataan huruf atau typesetting. Lorem Ipsum telah menjadi standar contoh teks sejak tahun 1500an, saat seorang tukang cetak yang tidak dikenal mengambil sebuah kumpulan teks dan mengacaknya untuk menjadi sebuah buku contoh huruf.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
  	</div>
  	<div class="col-md-4">
  		<div class="card card-blog">
        <div class="card-image">
          <a>
            <img class="img" src="<?=base_url()?>assets/img/pejabat.png" alt="" />
          </a>
        </div>
        <div class="content">
          <?php $hrg_pjbt = field_value('paket','id_paket','2','harga');?>
          <a class="btn btn-primary">Rp. <?=number_format($hrg_pjbt,2,",",".") ?></a>
          <div class="panel-group">
            <div class="panel panel-border panel-default">
              <div class="panel-heading" role="tab" id="heading2">
                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="false" aria-controls="collapse2">
                  <h4 class="panel-title">Detail<i class="material-icons">keyboard_arrow_down</i></h4>
                </a>
              </div>
              <div id="collapse2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2">
                <div class="panel-body">
                  <p>
                    Lorem Ipsum adalah contoh teks atau dummy dalam industri percetakan dan penataan huruf atau typesetting. Lorem Ipsum telah menjadi standar contoh teks sejak tahun 1500an, saat seorang tukang cetak yang tidak dikenal mengambil sebuah kumpulan teks dan mengacaknya untuk menjadi sebuah buku contoh huruf.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
  	</div>
  	<div class="col-md-4">
  		<div class="card card-blog">
        <div class="card-image">
          <a>
            <img class="img" src="<?=base_url()?>assets/img/pengusaha.png" alt="" />
          </a>
        </div>
        <div class="content">
          <?php $hrg_peng = field_value('paket','id_paket','3','harga');?>
          <a class="btn btn-primary">Rp. <?=number_format($hrg_peng,2,",",".") ?></a>
          <div class="panel-group">
            <div class="panel panel-border panel-default">
              <div class="panel-heading" role="tab" id="heading3">
                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse3" aria-expanded="false" aria-controls="collapse3">
                  <h4 class="panel-title">Detail<i class="material-icons">keyboard_arrow_down</i></h4>
                </a>
              </div>
              <div id="collapse3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3">
                <div class="panel-body">
                  <p>
                    Lorem Ipsum adalah contoh teks atau dummy dalam industri percetakan dan penataan huruf atau typesetting. Lorem Ipsum telah menjadi standar contoh teks sejak tahun 1500an, saat seorang tukang cetak yang tidak dikenal mengambil sebuah kumpulan teks dan mengacaknya untuk menjadi sebuah buku contoh huruf.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
  	</div>
  	<div class="col-md-4">
  		<div class="card card-blog">
        <div class="card-image">
          <a>
            <img class="img" src="<?=base_url()?>assets/img/pelajar.png" alt="" />
          </a>
        </div>
        <div class="content">
          <?php $hrg_pel = field_value('paket','id_paket','4','harga');?>
          <a class="btn btn-primary">Rp. <?=number_format($hrg_pel,2,",",".") ?></a>
          <div class="panel-group">
            <div class="panel panel-border panel-default">
              <div class="panel-heading" role="tab" id="heading4">
                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="false" aria-controls="collapse4">
                  <h4 class="panel-title">Detail<i class="material-icons">keyboard_arrow_down</i></h4>
                </a>
              </div>
              <div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading4">
                <div class="panel-body">
                  <p>
                    Lorem Ipsum adalah contoh teks atau dummy dalam industri percetakan dan penataan huruf atau typesetting. Lorem Ipsum telah menjadi standar contoh teks sejak tahun 1500an, saat seorang tukang cetak yang tidak dikenal mengambil sebuah kumpulan teks dan mengacaknya untuk menjadi sebuah buku contoh huruf.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
  	</div>
  	<div class="col-md-4">
  		<div class="card card-blog">
        <div class="card-image">
          <a>
            <img class="img" src="<?=base_url()?>assets/img/nirredaksi.png" alt="" />
          </a>
        </div>
        <div class="content">
          <?php $hrg_nir = field_value('paket','id_paket','5','harga');?>
          <a class="btn btn-primary">Rp. <?=number_format($hrg_nir,2,",",".") ?></a>
          <div class="panel-group">
            <div class="panel panel-border panel-default">
              <div class="panel-heading" role="tab" id="heading5">
                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse5" aria-expanded="false" aria-controls="collapse5">
                  <h4 class="panel-title">Detail<i class="material-icons">keyboard_arrow_down</i></h4>
                </a>
              </div>
              <div id="collapse5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading5">
                <div class="panel-body">
                  <p>
                    Lorem Ipsum adalah contoh teks atau dummy dalam industri percetakan dan penataan huruf atau typesetting. Lorem Ipsum telah menjadi standar contoh teks sejak tahun 1500an, saat seorang tukang cetak yang tidak dikenal mengambil sebuah kumpulan teks dan mengacaknya untuk menjadi sebuah buku contoh huruf.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
  	</div>
  </div>
</div>