<?php header('Content-type: application/xml; charset="ISO-8859-1"',true);  ?>
 
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
<url>
  <loc>http://bikinbuku.co.id/</loc>
  <priority>1.00</priority>
</url>
<url>
  <loc>http://bikinbuku.co.id/layanan</loc>
  <priority>0.80</priority>
</url>
<url>
  <loc>http://bikinbuku.co.id/persyaratan</loc>
  <priority>0.80</priority>
</url>
<url>
  <loc>http://bikinbuku.co.id/faq</loc>
  <priority>0.80</priority>
</url>
<url>
  <loc>http://bikinbuku.co.id/member/daftar</loc>
  <priority>0.80</priority>
</url>
</urlset>